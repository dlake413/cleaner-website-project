<?php

session_start();

if(isset($_SESSION['errorlist'], $_SESSION['previouspost'])){
    $error_post = $_SESSION['errorlist'];
    $previous_post = $_SESSION['previouspost'];
    unset($_SESSION['errorlist']);
    unset($_SESSION['previouspost']);
} elseif(isset($_SERVER['REQUEST_METHOD'])){
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        $uinfo = $_POST['user'];
        $ainfo = $_POST['address'];
        
        $_SESSION['userinfodata'] = $uinfo;
        $_SESSION['addressinfodata'] = $ainfo;
        $_SESSION['repass'] = $_POST['repass'];
    
        header("Location: backend/registration_validation.php");
    }
} else {
    $previous_post = array();
    for($i = 0; $i < 11; $i += 1){
        $previous_post[$i] = "";
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Le Pressing | Request Pickup</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
  </head>
<body>
<div id="container">
    
  <?php include_once "navigation.php"; ?>

    <div style="width:100%;height:30px;background-color:#000;"></div>
    
    <div id="main2">
    <div id="main2-cont" style="padding-top:40px;">
        <p style="text-align:center;font-size:3em;">Request a Pickup!</p>
        
        <center>
        <div style="text-align:left;width:1000px;">
        <form class='pickup_form' method='post' action='order.php'>

            <hr>
            First Name: <input type='text' name='first_name' value=''>* <br>
          	Last Name: <input type='text' name='last_name' value=''>*<br> 
			Email Address: <input type='text' name='email' value=''>*<br>
            <!-- We will need to figure out how to set the password security -->
            
            <hr>
			Address: <input type='text' name='address' value=''>*<br>
            City: <input type='text' name='city' value=''> *<br>
            State/Province: <input type='text' name='state' value=''>*<br>
            Zip/Postal Code: <input type='text' name='zip' value=''>*<br>
            Country: <input type='text' name='country' value=''>
			<br>
            Phone Number: <input type='text' name='phone' value=''>* <br>
            Would you like your shirts starched? 
            <select name='starch_select'>
				<option value="no">No</option>
				<option value="low">Low</option>
				<option value="medium">Medium</option>
				<option value="high">High</option>
			</select>
			<br>
			I want my shirts...
			<select name='hanging_select'>
				<option value="hanging">Hanging</option>
				<option value="boxed">Boxed</option>
			</select>
			<br> 
            Day for pick-up: 
			<select name='day_select'>
				<option value="monday">Monday</option>
				<option value="tuesday">Tuesday</option>
				<option value="wednesday">Wednesday</option>
				<option value="thursday">Thursday</option>
				<option value="friday">Friday</option>
				<option value="saturday">Saturday</option>
				<option value="sunday">Sunday</option>
			</select>
			<br>
            Doorman? 
			<select name='doorman_select'>
				<option value="yes">Yes</option>
				<option value="no">No</option>
			</select>
			<br>
			Best time to pick-up:
			<select name='time_select'>
				<option value='morning'>Morning</option>
				<option value='afternoon'>Afternoon</option>
			</select>
			<br>
			Special Instructions:
			<br>
			<textarea name="instructions" cols="20" rows="3" placeholder="Any special instructions...?"></textarea>
			<br>
            <input class='Button' type='submit' value='Request Pickup'> <a href='index.php'>Cancel</a>
        </form>        
        </div></center>
    </div>
    </div>
    
    <div id="map"></div>
   <?php include('footer.php'); ?> 
    
</div>
</body>
</html>
