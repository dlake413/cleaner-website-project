<?php
include_once 'backend/authentication.php';
include_once 'backend/back_office/db.php';
include_once 'backend/back_office/DAL.php';

session_start();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Le Pressing</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <link href="css/mypage.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
    <script>
    function resizeSlider() {
//        var w = $(window).width();
        document.getElementById('asdf').style.width = '1440px';
        alert("i got here");
    }
    </script>

  </head>
<body onload="resizeSlider()">
<div id="container">
    
<?php include "navigation.php"; ?>

<?php
  if(logincheck($db)){
    $user = readUser($db, $_SESSION['user_id'],false);
    $addresses = readUserAddrs($db, $user['id'], "");
    $address = readAddress($db, $addresses[0]['addrID']);
    $orders = readOrders($db, "%%", "", "", "", "", $user['id'], "", "");
    echo "
<div class='mypage'>
<h3> My Page </h3>
<div class='mpuinfo'>
<div class='left'>
<h4 style='margin:10px 0px 10px 10px;'>User Information</h4>
<hr style='margin-left:5px;'>
<p><span class='column'>Name</span> <span class='entry'>{$user['first_name']} {$user['last_name']}</span></p>
<p><span class='column'>Phone Number</span> <span class='entry'>{$user['phone_number']}</span></p>
<p><span class='column'>E-Mail</span> <span class='entry'>{$user['email']}</span></p>
<p><span class='column'>Address</span>
<span class='entry'>
{$address['street_address']}<br>
{$address['street_address2']}<br>
{$address['city']}, {$address['state']} {$address['zip']}<br>
</span></p>
</div>
<div class='right'>
<h4 style='margin:10px 0px 10px 10px;'>Order History</h4>
<hr style='margin-left:5px;'>";
    if(count($orders) == 0){
      echo "<p>This user has not placed any orders</p>";
    } else {
      echo "
<div class='orderhistory'><table class='ohist'>
<tr>
<th></th>
<th>Invoice No.</th>
<th>Order Date</th>
<th>Status</th>
</tr>
";
      for($i = 0; $i < count($orders); $i += 1){
	$order = $orders[$i];
	echo "
<tr>
<td>{$i}</td>
<td>{$order['id']}</td>
<td>{$order['order_date']}</td>
<td>{$order['order_status']}</td>
</tr>";
      }
      echo "</table></div>";
    }
   echo "
</div>
</div>
</div>
";
  } else {

    echo "
<p>You need to sign in to view this page.</p>
<p>Redirecting to the Sign in page in 5 seconds...</p>
<a href='login.php'>Click here if you're not redirected...</a>";
    header('refresh:5;url=login.php');
  }
?>
<?php include 'footer.php';?>
</div>
</body>
</html>