var map;
var map_canvas;

// Default center
var myLatlng = new google.maps.LatLng(40.7294468,-73.9863997);

// 1441 First Avenue (corner Of 75th St.) 212-249-5713
var loc1 = new google.maps.LatLng(40.7696511,-73.9546831);
    
// 843 Second Avenue (corner of 45th St.) 646-370-3022
var loc2 = new google.maps.LatLng(40.751986,-73.971048);
    
// 204 E. 10th Street (1st - 2nd Ave.) 212-477-7900
var loc3 = new google.maps.LatLng(40.7294468,-73.9863997);

// 1173 Second Avenue (62nd - 61st St.) 646-370-6887
var loc4 = new google.maps.LatLng(40.7624754,-73.9633736);
    
// 554 Third Avenue (36th - 37th St.) 212-706-7334
var loc5 = new google.maps.LatLng(40.7475449,-73.9772424);
    
// 2 South End Ave. (Battery Park) 212-786-4939
var loc6 = new google.maps.LatLng(40.707977,-74.0173492,17);

var mapStl = [{"featureType":"administrative",
               "elementType":"all",
               "stylers":[{"visibility":"on"},
                          {"saturation":-100},
                          {"lightness":20}]},
              {"featureType":"road",
               "elementType":"all",
               "stylers":[{"visibility":"on"},
                          {"saturation":-100},
                          {"lightness":40}]},
              {"featureType":"water",
               "elementType":"all",
               "stylers":[{"visibility":"on"},
                          {"saturation":-10},
                          {"lightness":30}]},
              {"featureType":"landscape.man_made",
               "elementType":"all",
               "stylers":[{"visibility":"simplified"},
                          {"saturation":-60},
                          {"lightness":10}]},
              {"featureType":"landscape.natural",
               "elementType":"all",
               "stylers":[{"visibility":"simplified"},
                          {"saturation":-60},
                          {"lightness":60}]},
              {"featureType":"poi",
               "elementType":"all",
               "stylers":[{"visibility":"off"},
                          {"saturation":-100},
                          {"lightness":60}]},
              {"featureType":"transit",
               "elementType":"all",
               "stylers":[{"visibility":"off"},
                          {"saturation":-100},
                          {"lightness":60}]}]

function initialize() {
    
    map_canvas = document.getElementById('map');
        
    var map_options = {
        center: myLatlng,
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: mapStl,

        disableDefaultUI: true,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        zoomControl: false,
        scaleControl: false,
        scrollwheel: false,
        disableDoubleClickZoom: true
    };
    
    map = new google.maps.Map(map_canvas, map_options);

    // 1441 First Avenue (corner Of 75th St.) 212-249-5713
    var m1 = new google.maps.Marker({
        position: loc1,
        animation: google.maps.Animation.DROP,
        map: map,
        title:"1441 First Avenue (corner Of 75th St.) 212-249-5713"
        });

    // 843 Second Avenue (corner of 45th St.) 646-370-3022
    var m2 = new google.maps.Marker({
        position: loc2,
        animation: google.maps.Animation.DROP,
        map: map,
        title:"843 Second Avenue (corner of 45th St.) 646-370-3022"
    });

    // 204 E. 10th Street (1st - 2nd Ave.) 212-477-7900
    var m3 = new google.maps.Marker({
        position: loc3,
        animation: google.maps.Animation.DROP,
        map: map,
        title:"204 E. 10th Street (1st - 2nd Ave.) 212-477-7900"
    });

    // 1173 Second Avenue (62nd - 61st St.) 646-370-6887
    var m4 = new google.maps.Marker({
        position: loc4,
        animation: google.maps.Animation.DROP,
        map: map,
        title:"1173 Second Avenue (62nd - 61st St.) 646-370-6887"
    });

    // 554 Third Avenue (36th - 37th St.) 212-706-7334
    var m5 = new google.maps.Marker({
        position: loc5,
        animation: google.maps.Animation.DROP,
        map: map,
        title:"554 Third Avenue (36th - 37th St.) 212-706-7334"
    });

    // 2 South End Ave. (Battery Park) 212-786-4939
    var m6 = new google.maps.Marker({
        position: loc6,
        animation: google.maps.Animation.DROP,
        map: map,
        title:"2 South End Ave. (Battery Park) 212-786-4939"
    });
    
    m1.setMap(map);
    m2.setMap(map);
    m3.setMap(map);
    m4.setMap(map);
    m5.setMap(map);
    m6.setMap(map);
    
    var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Symphony Maids</h1>'+
        '<div id="bodyContent">'+
        '<p>Located at 554 Third Avenue</p>'+
        '<p><b>Symphony Cleaners is dedicated to providing quality '+
        'service. Serving Manhattan since 1986.</b></p>'+
        '<p>Since 1986 Symphony Cleaners has been a family owned and '+
        'operated, full service dry cleaning establishment. With many'+
        ' locations in Midtown and the Upper East Side, our services '+
        'are recognized for delivering the highest quality, care and '+
        'courtesy.</p>'+
        '</div>'+
        '</div>';
    
    var infowindow = new google.maps.InfoWindow({
        content: contentString,
    });    
    
//    google.maps.event.addListener(marker, 'click', function() {
//        infowindow.open(map,marker);
//    });
    
//    infowindow.open(map, marker);
}

function changeLoc(num) {
    var zoom = 18;
    var nextLoc;
    
    if (num == 1) {
        nextLoc = loc1;
    } else if (num == 2) {
        nextLoc = loc2;
    } else if (num == 3) {
        nextLoc = loc3;
    } else if (num == 4) {
        nextLoc = loc4;
    } else if (num == 5) {
        nextLoc = loc5;
    } else if (num == 6) {
        nextLoc = loc6;
    } else {
        alert("Javascript problem");
    }
    
    map.setZoom(zoom);
    map.panTo(nextLoc);
}

function resetLoc() {
    map.setZoom(12);
    map.panTo(myLatlng);
    map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
}

google.maps.event.addDomListener(window, 'load', initialize);