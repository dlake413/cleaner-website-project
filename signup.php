<?php

session_start();

if(isset($_SESSION['errorlist'], $_SESSION['previouspost'])){
    $error_post = $_SESSION['errorlist'];
    $previous_post = $_SESSION['previouspost'];
    unset($_SESSION['errorlist']);
    unset($_SESSION['previouspost']);
} elseif(isset($_SERVER['REQUEST_METHOD'])){
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        $uinfo = $_POST['user'];
        $ainfo = $_POST['address'];
        
        $_SESSION['userinfodata'] = $uinfo;
        $_SESSION['addressinfodata'] = $ainfo;
        $_SESSION['repass'] = $_POST['repass'];
    
        header("Location: backend/registration_validation.php");
    }
} else {
    $previous_post = array();
    for($i = 0; $i < 11; $i += 1){
        $previous_post[$i] = "";
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Le Pressing | Sign up</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
  </head>
<body>
<div id="container">
    
  <?php include_once "navigation.php"; ?>

    <div style="width:100%;height:30px;background-color:#000;"></div>
    
    <div id="main2">
    <div id="main2-cont" style="padding-top:40px;">
        <p style="text-align:center;font-size:3em;">Customer Information</p>
        
        <center>
        <div style="text-align:left;width:1000px;">
        <form class='registration_form' method='post' action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>'>

            <h3>Account Information</h3>
            <hr>
            Username: <input type='text' name='user[0]' value='<?php echo $previous_post[0];?>'> <span class='Warn'>* Username must be 8-16 characters. Only Alphabets and Numbers allowed. Must contain at least one number. <?php if(isset($error_post)){echo $error_post[0];} ?></span><br>
            Password: <input type='password' name='user[1]' value='<?php echo $previous_post[1];?>'> <span class='Warn'>* Password must be at least 8 characters. <?php if(isset($error_post)){echo $error_post[1];} ?></span><br> 
            Retype Password: <input type='password' name='repass'> <span class='Warn'>* <?php if(isset($error_post)){echo $error_post[11];}?></span><br>

            <!-- We will need to figure out how to set the password security -->
            
            <h3>Personal Information</h3>
            <hr>
            First Name: <input type='text' name='user[2]' value='<?php echo $previous_post[2];?>'> <span class='Warn'>* <?php if(isset($error_post)){echo $error_post[2];} ?></span><br>
            Last Name: <input type='text' name='user[3]' value='<?php echo $previous_post[3];?>'> <span class='Warn'>* <?php if(isset($error_post)){echo $error_post[3];} ?></span><br>
            Email: <input type='text' name='user[4]' value='<?php echo $previous_post[4];?>'> <span class='Warn'>* <?php if(isset($error_post)){echo $error_post[4];} ?></span><br>
            Phone Number: <input type='number' name='user[5]' value='<?php echo $previous_post[5];?>'> <span class='Warn'>* Without Dash <?php if(isset($error_post)){echo $error_post[5];} ?></span><br>
            Street Address: <input type='text' name='address[0]' value='<?php echo $previous_post[6];?>'> <span class='Warn'>* <?php if(isset($error_post)){echo $error_post[6];} ?></span><br>
            Street Address 2: <input type='text' name='address[1]' value='<?php echo $previous_post[7];?>'><br>
            City: <input type='city' name='address[2]' value='<?php echo $previous_post[8];?>'> <span class='Warn'>* <?php if(isset($error_post)){echo $error_post[8];} ?></span><br>
            State: <input type='text' name='address[3]' value='<?php echo $previous_post[9];?>'> <span class='Warn'>* <?php if(isset($error_post)){echo $error_post[9];} ?></span><br>
            ZIP: <input type='number' name='address[4]' value='<?php echo $previous_post[10];?>'> <span class='Warn'>* <?php if(isset($error_post)){echo $error_post[10];} ?></span><br>
            <input class='Button' type='submit' value='Create Account'> <a href='logout.php'>Cancel Registration</a>
        </form>        
        </div></center>
    </div>
    </div>
    
    <div id="map"></div>
   <?php include('footer.php'); ?> 
    
</div>
</body>
</html>
