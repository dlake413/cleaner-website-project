<!DOCTYPE html>
<?php
include_once 'backend/authentication.php';
include_once 'backend/back_office/db.php';
include_once 'backend/back_office/DAL.php';
?>
<html>
<head>
    <title>Le Pressing - Services</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
    <script>
    function resizeSlider() {
//        var w = $(window).width();
        document.getElementById('asdf').style.width = '1440px';
        alert("i got here");
    }
    </script>

  </head>
<body onload="resizeSlider()">
<div id="container">
    
   <?php include_once "navigation.php" ?>
    <div style="width:100%;height:30px;background-color:#000;"></div>
    
    <div id="main2" style="height:180px;">
    <div id="main2-cont3">
        <p><b>Find a Le Pressing conveniently located in your neighborhood!</b><br />
With multiple locations throughout Manhattan, it is likely that there is a Le Pressing conveniently located near you. We pride ourselves on providing quality service at each of these locations and developing personal relationships with the residents of its unique neighborhood.
Which location can you call yours?</p>
    </div>
    </div>    
    
    <div id="map-container" style="position:relative;height:500px;overflow:hidden;">
        <div id="map" style="height:100%;width:100%;"></div>
        
        <!-- Left menu -->
        <div id="locmenu" style="left:0px;"><table>
            <tr><td onmouseover="changeLoc(1)" onmouseout="resetLoc()">
                <p>
                <b>Le Pressing</b><br />
                    1441 First Avenue (corner of 75th St.)<br />
                    212-249-5713<br />
                    <a id="getdirlink" href="https://www.google.com/maps/dir//1441+1st+Ave,+New+York,+NY+10021/" target="_blank">get directions</a>
                </p></td></tr>
            <tr><td onmouseover="changeLoc(2)" onmouseout="resetLoc()">                <p>
                <b>Le Pressing</b><br />
                    843 Second Avenue (corner of 45th St.)<br />
                    646-370-3022<br />
                    <a id="getdirlink" href="https://www.google.com/maps/dir//843+2nd+Ave,+New+York,+NY+10017/" target="_blank">get directions</a>
                </p></td></tr>

            <tr><td onmouseover="changeLoc(3)" onmouseout="resetLoc()">                <p>
                <b>Le Pressing</b><br />
                    204 East 10th Street (between 1st and 2nd Ave.)<br />
                    212-477-7900<br />
                    <a id="getdirlink" href="https://www.google.com/maps/dir//204+E+10th+St,+New+York,+NY+10003/" target="_blank">get directions</a>
                </p></td></tr>
            </table></div>

        <!-- Right menu -->
        <div id="locmenu" style="right:0px;"><table>
            <tr><td onmouseover="changeLoc(4)" onmouseout="resetLoc()">
                <p>
                <b>Le Pressing</b><br />
                    1173 Second Avenue (between 62nd and 61st St.)<br />
                    646-370-6887<br />
                    <a id="getdirlink" href="https://www.google.com/maps/dir//1173+2nd+Ave,+New+York,+NY+10065/" target="_blank">get directions</a>
                </p></td></tr>
            <tr><td onmouseover="changeLoc(5)" onmouseout="resetLoc()">                <p>
                <b>Le Pressing</b><br />
                    554 Third Avenue (between 36th and 37th St.)<br />
                    212-706-7334<br />
                    <a id="getdirlink" href="https://www.google.com/maps/dir//554+3rd+Ave,+New+York,+NY+10016/" target="_blank">get directions</a>
                </p></td></tr>

            <tr><td onmouseover="changeLoc(6)" onmouseout="resetLoc()">                <p>
                <b>Le Pressing</b><br />
                    2 South End Avenue (near Battery Park)<br />
                    212-786-4939<br />
                    <a id="getdirlink" href="https://www.google.com/maps/dir//2+South+End+Ave,+New+York,+NY+10280/" target="_blank">get directions</a>
                </p></td></tr>
            </table></div>
    
    </div>
    
    <div id="main2" style="height:300px;background-image:url(images/loc-banner.jpg);background-size:cover;background-position: center top;">
    <div id="main2-cont">
    </div>
    </div>    
        
    <div id="footer">
    <div id="footer-cont"><p>Copyright © 2014 Le Pressing Garment Care. All Rights Reserved. | 
        <a href="policy.html">Our Policies &amp; Terms of Agreement</a> | 
        204 E 10th St New York, NY 10003 | 212.477.7900 | 
        <a href="https://www.facebook.com/lepressingnyc" target="_blank">fb</a>&nbsp;
        <a href="https://twitter.com/lepressingnyc" target="_blank">tw</a>&nbsp;
        <a href="https://plus.google.com/116757499599350023104/" target="_blank">g+</a>
        </p></div>
    </div>
    
</div>
    

    
</body>
</html>
