<?php
include_once 'backend/authentication.php';
include_once 'backend/back_office/db.php';
include_once 'backend/back_office/DAL.php';

session_start();

?>
<div id="navi">
  <div id="navi-cont">
    <center>
      <table style="width:1000px;"><tr>
      <td style="text-align:left;"><a href="/sites/le-pressing/"><img src="images/logo.png" height="100px"></a></td>
      <td style="text-align:center;"><a href="about.php" id="navi-link">Our Company</a></td>
      <td style="text-align:center;"><a href="services.php" id="navi-link">Services</a></td>
      <td style="text-align:center;"><a href="locations.php" id="navi-link">Locations</a></td>
      <td style="text-align:center;"><a href="placeorder.php" id="navi-link">Request Pickup</a></td>
      <td style="text-align:center;"><a href="contact.php" id="navi-link">Contact</a></td>
          
<?php 
echo "<td style='text-align:center;'>";
if(logincheck($db)){
    $user = readUser($db, $_SESSION['user_id'], false);
    $u = $_SESSION['ug'];
    echo "Welcome, <b>{$user['first_name']} {$user['last_name']}</b>!<br />";
    if(isset($u)){
        if($u == 'a' || $u == 'w'){
            echo "<a style='text-decoration:none;color:#000;font-size:0.8em;' href='backend/back_office/order_data.php' id='navi-link'>Back Office</a> | ";
        } else {
            echo "<a style='text-decoration:none;color:#000;font-size:0.8em;' href='mypage.php' id='navi-link'>My Account</a> | ";
        }
    }
    echo "<a style='text-decoration:none;color:#000;font-size:0.8em;' href='logout.php' id='navi-link'>Sign out</a>";
} else {
    echo "<a id='navi-link' href='login.php'>( Sign in )</a>";
}
echo "</td>";

?>

      </tr></table>
    </center>
  </div>
</div>
