<!DOCTYPE html>
<?php
include_once 'backend/authentication.php';
include_once 'backend/back_office/db.php';
include_once 'backend/back_office/DAL.php';

session_start();
?>
<html>
<head>
    <title>Le Pressing | Policy</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
  </head>
<body>
<div id="container">
    
    <?php include_once "navigation.php"; ?>
    <div style="width:100%;height:30px;background-color:#000;"></div>
    
    <div id="main2">
    <div id="main2-cont" style="padding-top:40px;">
        <p style="text-align:center;font-size:3em;">Our Policy</p>
        
        <p><b>General Conditions</b></p>
        <p>Here at Le Pressing, we use the utmost care in processing articles entrusted 
            to us. Through our years of experience and extensive dry cleaning knowledge, we use 
            processes which we believe are best suited to the nature and condition of each 
            individual article. Nevertheless, we cannot assume responsibility for inherent weaknesses 
            or defects in materials that are not readily apparent prior to processing. This applies 
            particularly, but not exclusively, to suedes, leathers, silks, satins, double-faced fabrics, 
            vinyls, polyurethanes, lace, etc. Responsibility also is disclaimed for trimmings, buckles, 
            beads, buttons, bells and sequins. Likewise, although our staff is trained to meticulously 
            examine each garment for private property and valuables left inside garments, Le Pressing 
            is not responsible for any of such missing items. According to New York State law, we cannot 
            hold unclaimed garments for more than 3 months. As such, Le Pressing is not responsible 
            for items that have not been claimed after 3 months from the time of drop off. Unclaimed garments 
            beyond this period will be donated to charity accordingly. Le Pressing is not liable for 
            reimbursing new items purchased by the customer to replace lost or damaged items. Our liability 
            with respect to any lost or damaged item shall not exceed more than 15 times our charge for cleaning 
            the item, regardless of brand or condition.</p><br />
        
        <p><b>Dry Cleaning Damage Policy</b></p>
        <p>Although we take the utmost care in processing each individual article, damages may inevitably occur. 
            Our staff is trained to clean each garment with the utmost care and according to the guidelines set 
            forth by the manufacturer as described on the labels or tags of each individual item. Le Pressing 
            is not responsible for garments with inherent manufacturer defects or mislabeling of cleaning procedure. 
            Claims for damaged items must be reported within 48 hours from the time of pick up or delivery, 
            accompanied by the ticket and receipt or proof of purchase. Reimbursement therein will be issued in 
            accordance to the appraised value of each damaged item by following the International Fair Claims Guide 
            for Consumer Textile Products as set forth by the International Fabricare Institute, and shall not exceed 
            more than 15 times our charge for cleaning the item. If no receipt or proof of purchase is provided within 
            48 hours of the claim, the reimbursement issued shall not exceed more than 10 times our charge for 
            cleaning the item.</p><br />

        <p><b>Lost Item Policy</b></p>
        <p>Every measure is taken to ensure there is no loss of personal property. However, in the rare instance 
            that items are missing, a claim must be submitted within 48 hours from the time of pick up or delivery, 
            along with a receipt or proof of purchase. Reimbursement therein shall not exceed more than 15 
            times our charge for cleaning the item.</p><br />
        
        <p><b>Wash &amp; Fold Policy</b></p>
        <p>Reimbursements for lost or damaged items tendered through our Wash &amp; Fold laundering service may be 
            issued if a detailed list of each item is accompanied with the order at the time of drop off. The list 
            must include the brand, item style, color and size of each individual item in the order. The value of 
            the reimbursement for Wash &amp; Fold services shall not exceed more than 10 times our charge for cleaning 
            the order. If no list is provided at the time of drop off, Le Pressing is not responsible for any 
            lost or damaged items and there will be <u>NO REIMBURSEMENTS</u> or compensation of any form.</p><br />
        
        <p><b>Customer Agreement</b></p>
        <p>By purchasing our services, the customer agrees they are at risk at all times for the loss or damage of their 
            personal property. Therefore, the customer agrees any claims and reimbursements must be made in accordance 
            to Le Pressing’ Policies and the guidelines mentioned above.</p><br />
    </div>
    </div>
    
    <div id="map"></div>
    
    <div id="footer">
    <div id="footer-cont"><p>Copyright © 2014 Le Pressing Garment Care. All Rights Reserved. | 
        <a href="policy.php">Our Policies &amp; Terms of Agreement</a> | 
        204 E 10th St New York, NY 10003 | 212.477.7900 | 
        <a href="https://www.facebook.com/lepressingnyc" target="_blank">fb</a>&nbsp;
        <a href="https://twitter.com/lepressingnyc" target="_blank">tw</a>&nbsp;
        <a href="https://plus.google.com/116757499599350023104/" target="_blank">g+</a>
        </p></div>
    </div>
    
</div>
    

    
</body>
</html>
