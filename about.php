<!DOCTYPE html>
<?php
include_once 'backend/authentication.php';
include_once 'backend/back_office/db.php';
include_once 'backend/back_office/DAL.php';
?>
<html>
<head>
    <title>Le Pressing | Our Company</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
    <script>
    function resizeSlider() {
//        var w = $(window).width();
        document.getElementById('asdf').style.width = '1440px';
        alert("i got here");
    }
    </script>

  </head>
<body onload="resizeSlider()">
<div id="container">

   <?php include "navigation.php";?>    

    <div style="width:100%;height:30px;background-color:#000;"></div>
    
    <div style="width:100%;height:300px;background-image:url(images/pano-nyc.jpg);background-size:cover;"></div>

    
    <div id="main2">
    <div id="main2-cont" style="padding-top:40px;">
        <p style="text-align:center;font-size:3em;">Empire State of Mind</p>
        
        <p>Le Pressing (formerly Symphony Cleaners) is dedicated to providing quality 
            service for Manhattan since 1986. Our company has been a family owned and 
            operated, full service dry cleaning establishment. With many locations in 
            Midtown and the Upper East Side, our services are recognized for delivering 
            the highest quality, care and courtesy.</p>
        
        <p>The professional dry-cleaning service at Le Pressing keeps your clothing looking 
            like new. In addition to our laundry and dry cleaning services, we do tailoring 
            and alterations that will help you save money and time. Learn more about our 
            services by clicking on the "Services" tab at the top of the page. Our specialized 
            services include everything from Suede and Leather cleaning to Wedding Gown 
            cleaning and preservation.</p>
        
        <br /><br />
        <center>
            <hr width=1000px>
            <br />
            <p style="text-align:center;font-size:3em;">Testimonials</p>
            <p style="text-align:center;">We treat our customers like family and this is just what a few of them had to say.</p><br /><br />
            <table id="newstable">
            <tr>
                <td style="width:300px;overflow:hidden:">
                    <p><img src="images/testimonials/dukepark.jpg" height="200px" style="border-width:1px;border-color:#000;border-style:solid;"></p>
                    <p><b>Duke Park</b></p>
                    <p style="text-align:left;font-size:0.8em;">"I love Le Pressing! They have arguably the best prices around. 
                        You can't beat their complimentary pick-up and delivery!</p>
                    <p style="text-align:left;font-size:0.8em;">I would definitly recommend this place to anyone in a heartbeat."</p>
                </td>
                <td>
                    <p><img src="images/testimonials/johndoe.jpg" height="200px" style="border-width:1px;border-color:#000;border-style:solid;"></p>
                    <p><b>John Doe</b></p>
                    <p style="text-align:left;font-size:0.8em;">"Le Pressing is definitly the best in Manhattan. They really 
                        deliver what they sell. Could not ask for a better cleaners to keep my suits sharp."</p>
                </td>
                <td>
                    <p><img src="images/testimonials/janedoe.jpg" height="200px" style="border-width:1px;border-color:#000;border-style:solid;"></p>
                    <p><b>Jane Doe</b></p>
                    <p style="text-align:left;font-size:0.8em;">"Professional. That would be the word I would sum up Le Pressing.
                        I would follow that up with efficient and amazing. I love Le Pressing!"</p>
                </td>            
            </tr>
        </table>

        </center>
    </div>
    </div>
    
    <div id="main1" style="background-image:url(images/staff.jpg);background-position:top center;">
    <div id="main1-cont">
    </div>
    </div> 
   <?php include('footer.php') ?>         
    
</div>
    

    
</body>
</html>
