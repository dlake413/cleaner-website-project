<!DOCTYPE html>
<?php
include_once 'backend/authentication.php';
include_once 'backend/back_office/db.php';
include_once 'backend/back_office/DAL.php';
?>
<html>
<head>
    <title>Le Pressing | Services</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
    
  </head>
<body>
<div id="container">
    
   <?php include "navigation.php";?>

    <div style="width:100%;height:30px;background-color:#000;"></div>
    
    <div id="main2">
    <div id="main2-cont" style="padding-top:40px;">
        <p style="text-align:center;font-size:3em;">Your clothes, Our pleasure!</p>
        
        <p>Complimentary pick-up and delivery is just another part of our full-service 
            operation at Symphony. We pick-up and deliver all throughout Manhattan - 
            including the Upper East Side, Upper West Side, Midtown East and West, Chelsea, 
            Soho, Downtown and the Wall Street area. Schedule a pick-up and delivery today!</p>
        
        <center>
            <hr width=1000px>
            <table id="newstable">
            <tr>
                <td style="width:300px;overflow:hidden:">
                    <p><img src="images/dryclean.jpg" width="200px" style="border-width:1px;border-color:#000;border-style:solid;"></p>
                    <p><b>Dry Cleaning</b></p>
                    <p style="text-align:left;font-size:0.8em;">Our professional dry-cleaning service at Symphony keeps your clothing looking like new!</p>
                    <p style="text-align:left;font-size:0.8em;">We take pride in our exceptional quality. We treat each garment with the unique care it deserves. Our dry cleaning processes ensure that your garment retains its color, shape, and delicate texture.</p>
                </td>
                <td>
                    <p><img src="images/shirtservice.jpg" width="200px" style="border-width:1px;border-color:#000;border-style:solid;"></p>
                    <p><b>Shirt Service</b></p>
                    <p style="text-align:left;font-size:0.8em;">On a hanger, in a box, or simply handpressed - the choice is yours!</p>
                    <p style="text-align:left;font-size:0.8em;">Most shirts are able to go through the wash-and-press process, while more more delicate fabrics must be go through our extensive dry cleaning process. You choose the process, the delivery (hanger or box), and the feel - extra starch or none at all, it's up to you.</p>
                </td>
                <td>
                    <p><img src="images/washfoldlaundry.jpg" width="200px" style="border-width:1px;border-color:#000;border-style:solid;"></p>
                    <p><b>Wash &amp; Fold Laundry</b></p>
                    <p style="text-align:left;font-size:0.8em;">Bring in your bag of laundry, and we provide same-day wash and fold service.</p>
                    <p style="text-align:left;font-size:0.8em;">Weighed by the pound, we take your bag of laundry and clean them for you! Your laundry goes through the typical washer-dryer process and is then folded neatly into our delivery bags.</p>
                </td>            
            </tr>
        </table>
            </center>
        </div>
        
        <div id="main2-cont" style="padding-top:0px;">
            <center>
                <p style="text-align:center;font-size:3em;">Pricing</p>
                <hr width=1000px>

            <div style="width:1230px;padding-bottom:100px;">
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/shirt.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Shirts $6</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/tie.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Ties $6</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/vest.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Vests $6</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/sweater.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Sweaters $6</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/pants.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Pants $6</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/shorts.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Shorts $6</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/jeans.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Jeans $6</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/blouse.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Blouses $6</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/top.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Tops $6</p></div></div>
                <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/scarf.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Scarfs $6</p></div></div>
            </div>
                
            <div style="width:800px;padding-bottom:100px;">
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/jacket.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Jacket $8</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/suit.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>2-pc Suit $14</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/dress.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Dress $13</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/outerjacket.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Outer Jacket $15</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/wintercoat.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Winter Coat $20</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/downcoat.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Down Coat $25</p></div></div>
            </div>
            
            <div style="width:1230px;padding-bottom:100px;">
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/comforter.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Comforter $35</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/pillowcase.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Pillow Cases $15</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/duvetcover.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Duvet Cover $25</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/tablecover.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Table Covers $25</p></div></div>
            <div id="pricing-button-container">
                <div id="pricing-button-image" style="background-image:url(images/pricing/bedsheet.jpg);"></div>
                <div id="pricing-button-hover"></div> 
                <div id="pricing-button-title"><p>Bed Sheets $20</p></div></div>
            </div>
                
            <div style="width:1230px;padding-bottom:100px;">
                <p><b>Other Services</b>
                <br />Wash &amp; Fold $1.00 / lb (Starting at $15)
                <br />Laundered Shirts on Hanger $2 per shirt (no startch, low, medium, or high starch)
                <br />Hand pressed shirts $5</p>
            </div>                
                
            </center>
        </div>            
    </div>
    
    <div id="main1" style="background-image:url(images/earthfriendly.jpg);background-position:top center;">
    <div id="main1-cont">
    </div>
    </div> 
  <?php include('footer.php'); ?>          
    
</div>
    

    
</body>
</html>
