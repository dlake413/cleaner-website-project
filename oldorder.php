<html>
<h1> LePressing Order Form </h1>
<!-- <form method="POST"action="*"name="orderform">
<table border="0" cellpadding="0" width="550" id="table1">
<tr>
<td width="340" align="right"><font color="#FF0000">Name</font></td>
<td width="10">&nbsp;</td>
<td width="200"><input type="text" name="Name" size="30" tabindex="1"></td>
</tr>
<tr>
<td width="340" align="right"><font color="#FF0000">Email</font>
(Your confirmation will be sent here): </td>
<td width="10">&nbsp;</td>
<td width="200"><input type="text" name="Email" size="30" tabindex="1"></td>
</tr>
<tr>
<td width="340" align="right">Other Contact Info:</td>
<td width="10">&nbsp;</td>
<td width="200"><input type="text" name="OtherInfo" size="30" tabindex="1"></td>
</tr>
<tr>
<td width="340" align="right">&nbsp;</td>
<td width="10">&nbsp;</td>
<td width="200">&nbsp;</td>
</tr>
</table>
-->

<fieldset id="lepressing-name" class="form-item fields name required">
                <div class="title">Name <span class="required">*</span></div>
                <legend>Name</legend>
                
                  <div class="field first-name">
                    <label class="caption"><input class="field-element field-control" name="fname"
                    x-autocompletetype="given-name" type="text"
                    spellcheck="false"
                    maxlength="30"
                    data-title="First" />
                    First Name</label>
                  </div>
                  <div class="field last-name">
                    <label class="caption"><input class="field-element field-control" name="lname"
                    x-autocompletetype="surname" type="text"
                    spellcheck="false" maxlength="30" data-title="Last" />
                    Last Name</label>
                  </div>
                </fieldset>

              
                <fieldset id="lepressing-address" class="form-item fields address required">
                  <div class="title">Address <span class="required">*</span></div>
                  <legend>Address</legend>
                  
                  <div class="field address1">
                    <label class="caption"><input class="field-element field-control" name="address"
                    x-autocompletetype="address-line1" type="text"
                    spellcheck="false" data-title="Line1" />
                    Address 1</label>
                  </div>
                  <div class="field address2">
                    <label class="caption"><input class="field-element field-control" name="address2" x-autocompletetype="address-line2" type="text"
                    spellcheck="false" data-title="Line2" />
                    Address 2</label>
                  </div>
                  <div class="field city">
                    <label class="caption"><input class="field-element field-control" name="city" x-autocompletetype="city"
                    type="text"
                    spellcheck="false" data-title="City" />
                    City</label>
                  </div>
                  <div class="field state-province">
                    <label class="caption"><input class="field-element field-control" name="state" x-autocompletetype="state"
                    type="text"
                    spellcheck="false" data-title="State" />
                    State/Province</label>
                  </div>
                  <div class="field zip">
                    <label class="caption"><input class="field-element field-control" name="zipcode"
                    x-autocompletetype="postal-code" type="text"
                    spellcheck="false" data-title="Zip" />
                    Zip/Postal Code</label>
                  </div>
                  <div class="field country">
                    <label class="caption"><input class="field-element field-control" name="country"
                    x-autocompletetype="country"
                    type="text"
                    spellcheck="false" data-title="Country" />
                    Country</label>
                  </div>
                </fieldset>
              

              

              
                <div id="lepressing-email" class="form-item field email required">
                  <label class="title" for="email-yui_3_10_1_1_1388682354667_33586-field">Email Address <span class="required">*</span></label>
                  
                  <input class="field-element" name="email" x-autocompletetype="email" type="text" spellcheck="false" id="email-yui_3_10_1_1_1388682354667_33586-field" />
                </div>
              


              
                <fieldset id="lepressing-phone" class="form-item fields phone required">
                <div class="title">Mobile Phone <span class="required">*</span></div>
                <legend>Mobile Phone</legend>
                
                  
                  <div class="field text three-digits">
                    <label class="caption"><input class="field-element" x-autocompletetype="phone-area-code" type="text"
                    maxlength="3" data-title="Areacode" />
                    (###)</label>
                  </div>
                  <div class="field text three-digits">
                    <label class="caption"><input class="field-element" x-autocompletetype="phone-local-prefix" type="text"
                    maxlength="3" data-title="Prefix" />
                    ###</label>
                  </div>
                  <div class="field text four-digits">
                    <label class="caption"><input class="field-element" x-autocompletetype="phone-local-suffix" type="text"
                    maxlength="4" data-title="Line" />
                    ####</label>
                  </div>
                </fieldset>
              

              
                <div id="starchselect" class="form-item field select">
                  <label class="title" for="starchselect">Would you like your shirts starched?</label>
                  <div class="description">We recommend your shirts be treated with light starch to ensure the longevity of the fabric</div>
                  <select name="starchselect">
                    
                      <option value="No Starch">No Starch</option>
                    
                      <option value="Light">Light</option>
                    
                      <option value="Medium">Medium</option>
                    
                      <option value="Heavy">Heavy</option>
                    
                  </select>
                </div>
              

              
                <div id="selecthangbox" class="form-item field select">
                  <label class="title" for="selecthangbox">Would you like your shirts..</label>
                  
                  <select name="selecthangbox">
                    
                      <option value="Hanging">Hanging</option>
                    
                      <option value="Boxed">Boxed</option>
                    
                  </select>
                </div>
              


              
                <div id="pickupday" class="form-item field checkbox">
                  <div class="title" for="pickupday-field">Pick-up day?</div>
                  <div class="description">Please allow for our standard three-day turnaround. We do not cut corners, and appreciate your patience while we beautifully clean your garments. If your order is absolutely time sensitive, please indicate below in your special instructions. Speciality items may require additional time</div>
                  
                  <div class="option"><label><input type="checkbox" name="pickupday-field" value="Monday"/> Monday</label></div>
                  
                  <div class="option"><label><input type="checkbox" name="pickupday-field" value="Tuesday"/> Tuesday</label></div>
                  
                  <div class="option"><label><input type="checkbox" name="pickupday-field" value="Wednesday"/> Wednesday</label></div>
                  
                  <div class="option"><label><input type="checkbox" name="pickupday-field" value="Thursday"/> Thursday</label></div>
                  
                  <div class="option"><label><input type="checkbox" name="pickupday-field" value="Friday"/> Friday</label></div>
                  
                  <div class="option"><label><input type="checkbox" name="pickupday-field" value="Saturday"/> Saturday</label></div>
                  
                </div>

              

              
                <div id="select-doorman" class="form-item field select">
                  <label class="title" for="select-yui_3_10_1_1_1388682354667_55209-field">Doorman Building?</label>
                  
                  <select name="select-doorman-field">
                    
                      <option value="Yes">Yes</option>
                    
                      <option value="No">No</option>
                    
                  </select>
                </div>

              
                <div id="select-time" class="form-item field select">
                  <label class="title" for="select-time-field">No Doorman? Best time to pick-up</label>
                  
                  <select name="select-time-field">
                    
                      <option value="8-10AM">8-10AM</option>
                    
                      <option value="10-12PM">10-12PM</option>
                    
                      <option value="12-2PM">12-2PM</option>
                    
                      <option value="2-4PM">2-4PM</option>
                    
                      <option value="4-6PM">4-6PM</option>
                    
                  </select>
                </div>

              

              

              
                <div id="instructions" class="form-item field textarea">
                  <label class="title" for="instructions-field">Special Instructions</label>
                  
                  <textarea class="field-element " id="instructions-field" ></textarea>
                </div>

          </div>
		
		<div class="form-button-wrapper">
          <input class="button sqs-system-button sqs-editable-button" type="submit" value="Submit"/>
        </div>

</html>
