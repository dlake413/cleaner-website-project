<?php
ini_set("display_errors",1);
error_reporting(-1);
include_once("backend/back_office/db.php");
$link=mysqli_connect('localhost', 'root', 'root', 'lepressing') or die("Error " . mysqli_error($link));

$requiredfilledup = True;
//echo 'mysqli_real_escape_string($link, $_POST['instructions']);'; 

function xss_check($data){
      if (preg_match('/[^a-zA-Z_\-\d\s\\n]+/', $data)) {
          $data = preg_replace('/[^a-zA-Z_\-\d\s\\n]+/','',$data);
  //exit($data.'Invalid input, can only contain letters A-Z, a-z, 0-9, and underscore\n');
      }
}


foreach($_POST as $value){
	//echo $value . ' ' . strlen($value) . "\r\n";
	xss_check($value);
	if(strlen($value) == 0){
		$requiredfilledup = False;
		break;
	}
}

if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
	$requiredfilledup = False;
}

$phone = preg_replace('/[\-]/', '', $_POST['phone']);

if(strlen($_POST['zip']) != 5){
	$requiredfilledup = False;
}

if($requiredfilledup){

require_once("backend/PHPMailer_5.2.4/class.phpmailer.php");
//SQL Injection protection
$firstname = mysqli_real_escape_string($link, $_POST['first_name']);
$lastname = mysqli_real_escape_string($link, $_POST['last_name']);
$email = mysqli_real_escape_string($link, $_POST['email']);
$address = mysqli_real_escape_string($link, $_POST['address']);
$city = mysqli_real_escape_string($link, $_POST['city']);
$state = mysqli_real_escape_string($link, $_POST['state']);
$zip = mysqli_real_escape_string($link, $_POST['zip']);
$country = mysqli_real_escape_string($link, $_POST['country']);
$phone = mysqli_real_escape_string($link, $_POST['phone']);
$starch = mysqli_real_escape_string($link, $_POST['starch_select']);
$hanging = mysqli_real_escape_string($link, $_POST['hanging_select']);
$day = mysqli_real_escape_string($link, $_POST['day_select']);
$doorman = mysqli_real_escape_string($link, $_POST['doorman_select']);
$time = mysqli_real_escape_string($link, $_POST['time_select']);
$instruction = mysqli_real_escape_string($link, $_POST['instructions']);
$instructions = htmlspecialchars($instruction);

$sql = "INSERT into pickup(first_name, last_name, address_1, city, state_province, zip, country, email, phone, starch, shirts_hanging, pickup_day, doorman, timepickup, special_instructions) VALUES ('$firstname', '$lastname', '$email', '$address', '$city', '$state', '$zip', '$country', '$phone', '$starch', '$hanging', '$day', '$doorman', '$time', '$instructions')";
if(!mysqli_query($link, $sql))
{
	die('Oops! An error occurred: ' .mysqli_error($link));
}

$mail2 = new PHPMailer();
$mail2->IsSMTP();
$mail2->SMTPDebug = 1;
$mail2->SMTPAuth = true;
$mail2->SMTPSecure='ssl';
$mail2->Host = "smtp.gmail.com";
$mail2->Port = 465;
$mail2->IsHTML(true);
$mail2->SetFrom = "Le-Pressing";
$mail2->Username = "hsuperior453@gmail.com";
$mail2->Password = "lepressing";
$mail2->AddAddress('hsuperior453@gmail.com');
$mail2->From = $email;
$mail2->Subject = "Pickup Request";


$emailSubject = 'LE-PRESSING Pick-up Request!';
$mailTo = 'hsuperior453@gmail.com';
$message_body = "Name: ".$firstname. $lastname."<br>";
$message_body .= "Phone: ".$phone."<br>";
$message_body .= "Email: ".$email."<br>";
$message_body .= "Address: ".$address."<br>";
$message_body .= "City: ".$city."<br>";
$message_body .= "State: ".$state."<br>";
$message_body .= "Zip: ".$zip."<br>";
$message_body .= "Starch: ".$starch."<br>";
$message_body .= "Hanging: ".$hanging."<br>";
$message_body .= "Pickup Day: ".$day."<br>";
$message_body .= "Best Pickup Time: ".$time."<br>";
$message_body .= "Special Instructions: ".$instructions."<br>";

$mail2->Body = $message_body;

if(!$mail2->Send()){
	echo 'Message could not be sent.';
	echo 'Mailer Error: ' . $mail2->ErrorInfo;
	exit;
}


header( "refresh:2;url=index.php" );
echo $instructions;
echo "<br>";
echo "Your request has been sent!";
echo "<br>";
echo 'You\'ll be redirected in about 2 seconds';

} else{
	header( "refresh:2;url=placeorder.php" );
	echo "You did not enter the required fields or you entered them incorrectly, please check your fields. Redirecting you back to your request...";
} 
?>
