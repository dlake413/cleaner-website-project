<!DOCTYPE html>
<?php
include_once 'backend/authentication.php';
include_once 'backend/back_office/db.php';
include_once 'backend/back_office/DAL.php';
?>
<html>
<head>
    <title>Le Pressing | Contact</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
    <script>
    function resizeSlider() {
//        var w = $(window).width();
        document.getElementById('asdf').style.width = '1440px';
        alert("i got here");
    }
    </script>

  </head>
<body onload="resizeSlider()">
<div id="container">
    
   <?php include "navigation.php"; ?>

    <div style="width:100%;height:30px;background-color:#000;"></div>
    <div style="height:200px;width:100vw;margin:0 auto;background-image:url(images/bkgd2.jpg);background-position:center top;background-size:cover;"></div>
    
    <div id="main2">
    <div id="main2-cont" style="padding-top:25px;">
        <p style="text-align:center;font-size:3em;">We'd love to hear from you!</p>
        <center><table>
        <tr>
        
            <td style="padding:50px;padding-top:20px;border-width:1px;border-color:#000;border-right-style:solid;">
                Address: <br />204 E 10th St New York, NY 10003<br /><br />
                Phone: <br />212.477.7900<br /><br />
                
            <form id="contactform" accept-charset="utf-8" action="http://www.foxyform.com/form.php?id=579730&mail=send&sec_hash=c0f7a5ff863&PHPSESSID=372cc951a9319003fa8d7c27171257e4" method="post" style="width: 100%;" novalidate>
                <input type="hidden" name="a984b9f6aeb4dfb9d083fdfb432f1dfc20aa4391" value="bd7b28cfa21b435e2d5801527616f3194d083b4767efda7b8d0b5847b76074c6" />
                E-mail:<br />
                <input type="email" value="someone@example.com" name="email" id="Email" 
                       onblur="if (this.value == '') {this.value = 'someone@example.com';}"
                       onfocus="if (this.value == 'someone@example.com') {this.value = '';}"><br />
                <textarea name="nachricht" class="form" onblur="if (this.value == '') {this.value = 'I love your service!';}"
                          onfocus="if (this.value == 'I love your service!') {this.value = '';}">I love your service!</textarea><br>
                AntiSpam Protection:
                <img src="http://www.foxyform.com/captcha/captcha.php?color=ffffff&PHPSESSID=372cc951a9319003fa8d7c27171257e4" title="Send message!" style="margin-top: 10px; border: none;"><br />
                <input type="text" name="sicherheitscode" class="captcha" size="6" maxlength="5"><br />
                <input type="submit" class="formsubmit" value="Send">
                </form>
            </td>
            <td style="padding:50px;padding-top:20px;">
            <p style="width:400px;">We pride ourselves in providing the best quality service to our customers. If, for any reason, 
                you are not satisfied, do not hesitate to contact us. We will work with you to find a solution. 
                You, our customer, are dear to us and we strive to continue to build a great relationship with 
                you. If you have any questions or comments (or praises!), call the numbers above or fill out 
                the box below. We would be happy to make your day a little bit easier.</p>
            </td>
            
        </tr>
        </table></center>
    </div>
    </div>
    
    <div id="map"></div>
        
   <?php include('footer.php'); ?> 
</div>
    

    
</body>
</html>
