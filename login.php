<?php

include_once 'backend/back_office/db.php';
include_once 'backend/authentication.php';

session_start();

if(logincheck($db)){
  if(isset($_SERVER['HTTP_REFERER'])){
    header('Location: ' . $_SERVER['HTTP_REFERER']);    
  } else {
    header('Location: /le-pressing');
  }
} else {
  $logged = "NOT LOGGED";
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Le Pressing | Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
  </head>
<body>
<div id="container">
  
  <?php include 'navigation.php'; ?>
    
    <div style="width:100%;height:30px;background-color:#000;"></div>
    <div style="height:200px;width:100vw;margin:0 auto;background-color:#fff;background-image:url(images/shirts.jpg);background-position:left middle;background-size:contain;background-repeat:no-repeat;"></div>
    
    <div id="main2">
    <div id="main2-cont" style="padding-top:25px;">
        <p style="text-align:center;font-size:3em;">How may I help you today?</p>
        <center><table>
        <tr>
        
            <td style="padding:50px;padding-top:20px;border-width:1px;border-color:#000;border-right-style:solid;">
            
                <form id="contactform" method='post' action='backend/login_process.php'>
                    Username:<br />
                    <input type="text" name="username"><br />
                    Password:<br />
                    <input type='password' name='p'><br />
                    <input type="submit" value="Login">
                </form>
            </td>
            <td style="padding:50px;padding-top:20px;">
                <p style="width:400px;"><b>Are you new here?</b><br />
                    Create a Le Pressing account today to reap the benefits of a personalized 
                    ordering experience. With a Le Pressing account, you'll be able to conveniently 
                    manage your account, check the status of any recent orders and much more.</p>
                <br /><a href="signup.php" style="color:#000;">I'm ready, lets go.</a>
            </td>
            
        </tr>
        </table></center>
    </div>
    </div>
    
   <?php include('footer.php'); ?> 
</div>
    
<?php
  if(isset($_SESSION['ERROR'])){
    $error = $_SESSION['ERROR'];
    echo 'Could not validate the login information. <br> Please try again: '.$error;
    unset($_SESSION['ERROR']);
    session_regenerate_id(true);
  }
?>
    
</body>
</html>
