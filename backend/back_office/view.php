<!--include all the php search functions and displays.-->
<?php
//DEFINE
/*
  showUserTable: User Search Function
  db: mysqli
  id:  user id to search
  pfn: first name to search
  pln: last name to search
  pem: email to search
  pnn: phonenumber to search
  pug: usergroup search
*/
include("DAL.php");

function showUserTable($db, $pid, $pfn, $pln, $pem, $ppn, $pug) {
  
  echo "<table class='user'>\n
          <tr>\n
            <th>User Id</th>\n
            <th>First Name</th>\n
            <th>Last Name</th>\n
            <th>Phone Number</th>\n
            <th>E-Mail</th>\n
            <th>User Type</th>\n
            <th>Join Date</th>\n
            <th>Detail</th>\n
          </tr>\n";
        
  //extract data;

  $array = readUsers($db, "%%", "", "", "", "", "", "", "", "");

  foreach($array as $data){
    if($data['user_group'] == 'a'){
      $ug = "Administrator";
    } elseif($data['user_group'] == 'w'){
      $ug = "Worker";
    }
    else {
      $ug = "Customer";
    }
    echo "<tr>\n
            <td><button>x</button>\n
            <td>{$data['first_name']}</td>\n
            <td>{$data['last_name']}</td>\n
            <td>{$data['phone_number']}</td>\n
            <td>{$data['email']}</td>\n
            <td>{$ug}</td>\n
            <td>{$data['created_on']}</td>\n
            <td><center><a href='user_detail.php?uid={$data['id']}'>View</a></center></td>
          </tr>\n";
  }
  
  echo "</table>";
}

/*
  
*/

function showOrderDetail($db, $oid){
  $order = readOrder($db, $oid);
  
  if(gettype($order) == "string"){
    echo "<p>{$order}</p>";
  } else {
    $user = readUser($db, $order['customer_id'], false);
    $address = readAddress($db, $order['addrID']);
    $zip = sprintf('%05d', $address['zip']);
    $invoices = readInvoices($db, '%%', "", "",  $order['id']);

    echo
      "
<div class='orderinfo'>
<h3>Order Information</h3>
<div class='left'>
<p><span class='column'><b>Invoice Number</b></span> <span class='value'>{$order['id']}</span></p>
<p><span class='column'><b>Order Date</b></span> <span class='value'>{$order['order_date']}</span> </p>
<p><span class='column'><b>Order Status</b></span> <span class='value'>{$order['order_status']}</span></p>
<p><span class='column'><b>Finish Date</b></span> <span class='value'>{$order['finish_date']}</span></p>
<p><span class='column'><b>Cleaner Location</b></span> <span class='value'>{$order['location_id']}</span></p>
</div>
<div class='right'>
<p><span class='column'><b>Customer Name</b></span> <span class='value'><a href='user_detail.php?uid={$user['id']}'>{$user['first_name']} {$user['last_name']}</a></span></p>
<p><span class='column'><b>Phone Number</b></span> <span class='value'>{$user['phone_number']}</span></p>
<p><span class='column'><b>Email</b></span> <span class='value'>{$user['email']}</span></p>
<p><span class='column'><b>Address</b></span><span class='value'>{$address['street_address']}<br>{$address['street_address2']}<br>{$address['city']}, {$address['state']}<br>{$zip}</span></p>

</div></div>";

    echo "
<div class='item'>
<hr>
<h3>Order Detail</h3>
";
    if(count($invoices) > 0) {
      echo "
<table class='items' style='margin-left:50px;'>
<tr>
<th style='width:60px;'></th>
<th style='width:300px;'>Type Services</th>
<th style='width:100px;'>No. Items</th>
<th style='width:75px;'>Charge</th>
</tr>
";
      $total = 0.00;
      for($i = 0; $i < count($invoices); $i += 1){
	$data = $invoices[$i];	
	$service = readService($db, $data['service_id']);
	$price = sprintf('%01.2f', $data['number_items'] * $service['price']);
       	echo "
<tr>
<td>{$i}</td>
<td>{$service['type_service']}</td>
<td>{$data['number_items']}</td>
<td> {$price}</td>
</tr>
";
	$total += $price;
      }
      $total = sprintf('%01.2f', $total);
      echo "
<tr>
<td>TOTAL</td>
<td style='border:none'></td>
<td style='border:none'></td>
<td>{$total}</td>
</tr>
</table>
";
    } else {

      echo "<p>This is an empty order</p>";

    }
      echo "</div>";
    }
}

function orderEdit($db, $oid){
  $order = readOrder($db, $oid);
  
  if(gettype($order) == "string"){
    echo "<p>{$order}</p>";
  } else {
    $user = readUser($db, $order['customer_id'], false);
    $address = readAddress($db, $order['addrID']);
    $zip = sprintf('%05d', $address['zip']);
    $invoices = readInvoices($db, '%%', "", "",  $order['id']);

    echo
      "
<form method='post' 
action='order_edit_submit.php'>
<div class='orderinfo'>
<h3>Order Information</h3>
<div class='left'>
<p><span class='column'><b>Invoice Number</b></span> <span class='value'>{$order['id']}</span></p>
<p><span class='column'><b>Order Date</b></span> <span class='value'>{$order['order_date']}</span> </p>
<p><span class='column'><b>Order Status</b></span> <span class='value'>{$order['order_status']}</span></p>
<p><span class='column'><b>Finish Date</b></span> <span class='value'>{$order['finish_date']}</span></p>
<p><span class='column'><b>Cleaner Location</b></span> <span class='value'>{$order['location_id']}</span></p>
</div>
<div class='right'>
<p><span class='column'><b>Customer Name</b></span> <span class='value'><a href='user_detail.php?uid={$user['id']}'>{$user['first_name']} {$user['last_name']}</a></span></p>
<p><span class='column'><b>Phone Number</b></span> <span class='value'>{$user['phone_number']}</span></p>
<p><span class='column'><b>Email</b></span> <span class='value'>{$user['email']}</span></p>
<p><span class='column'><b>Address</b></span><span class='value'>
{$address['street_address']}<br>
{$address['street_address2']}<br>
{$address['city']}, {$address['state']}<br>
{$zip}<br>
<button type='button' onClick='showAddressList()'>Different Address</button> <button type='button'>New Address</button></span></p>
<div id='useraddress' class='addresslist'>"; 

    $useraddrlist = readUserAddrs($db, $user['id'], "");
    $i = 0;
    echo "<form method='post' action='order_edit_submit.php'>";
    foreach($useraddrlist as $uad){
      echo "<input id='selectAddress{$i}' type='radio' name='selectAddress' value={$uad['addrID']}>";
      $address = readAddress($db, $uad['addrID']);
      echo "<label for='selectAddress{$i}'>{$address['street_address']}. {$address['city']}</label><br>";
      $i += 1;
    }
    
    echo 
"
<input type='submit' name='action' value='addressChange'></form>
</div></div></div>";

    echo "
<div class='item'>
<hr>
<h3>Order Detail</h3>
<table class='items' style='margin-left:50px;'>
<tr>
<th style='width:60px;'></th>
<th style='width:300px;'>Type Services</th>
<th style='width:100px;'>No. Items</th>
<th style='width:75px;'>Charge</th>
<th style='width:50px;'>Delete</th>
</tr>
";
    $total = 0.00;
    $i = 0;
    for($i; $i < count($invoices); $i += 1){
      $data = $invoices[$i];	
      echo "
<tr id='service{$i}'>
<td id='index{$i}'>{$i}</td>
<td id='serviceid{$i}'>{$data['service_id']}</td>
<td><input id='number{$i}' class='numberInput' type='number' min='0' name='numberItem' value='{$data['number_items']}'></td>
<td id='charge{$i}'>{$data['total_price']}</td>
<td><input name='delete{$i}' type='checkbox'></td>
</tr>
";
      $total += $data['total_price'];
    }
   
    $total = sprintf('%01.2f', $total);
    echo "
<tr id='newItem'>
<td></td>
<td style='border-left:none; border-right:none;'><button id='addbutton' type='button' onClick='addNewItem({$i})'>Add...</button></td>
<td style='border-left:none; border-right:none;'></td>
<td style='border-left:none;'></td>
<td />
</tr>
<tr>
<td>TOTAL</td>
<td style='border:none'></td>
<td style='border:none'></td>
<td>{$total}</td>
<td />
</tr>
</table>
</div></form>";
  }
}

    
function showUserDetail($db, $uid){
  $user = readUser($db, $uid, true);
  
  if(gettype($user) == "string"){
    echo "<p>{$user}</p>";
  } else {
    
    $addressbook = readUserAddrs($db, $uid, "");
    
    if($user['user_group'] == 'a'){
      $ug = 'Administrator';
    } else if($user['user_group'] == 'w'){
      $ug = 'Worker';
    } else {
      $ug = 'User';
    }

    if($user["activeflg"]){
      $active = "<span style='color:#0F0'><b>Active</b></span>";
    } else {
      $active = "<span style='color:#F00'><b>Inactive</b></span>";
    } 

    //Everything on the Left Panel is the User Information
    echo 
      "
  <div class='user_info'>
  <h3>User Information</h3>
  <div class='left'>
    <p><span class='column'><b>Name</b></span> 
       <span class='value'>{$user['first_name']} {$user['last_name']}</span></p>
    <p><span class='column'><b>Email</b></span>
       <span class='value'>{$user['email']}</span></p> 
    <p><span class='column'><b>Phone Number</b></span>
       <span class='value'>{$user['phone_number']}</span></p>
";
  
    foreach($addressbook as $ad){
      $address = readAddress($db, $ad['addrID']);
      echo "<div class='addressline'><p><span class='column'><b>Address</b></span>";
      echo "<span class='value'>{$address['street_address']}<br>{$address['street_address2']}<br>{$address['city']}, {$address['state']}<br>{$address['zip']}</span>";
      echo "</p></div>";
	
    }
    
    //Everything on the Right Panel is the Account Information
    echo "
  </div>
  <div class='right'>
    <p><span class='column'><b>Username</b></span> <span class='value'>{$user['username']}</span></p>
    <p><span class='column'><b>Level</b></span> <span class='value'>{$ug}</span></p>
    <p><span class='column'><b>Sign-up Date</b></span> <span class='value'>{$user['created_on']}</span></p>
    <p><span class='column'><b>Status</b></span> <span class='value'>{$active}</span></p>
  </div>
  </div>

<div class='orderhistory'><hr>
<h3>Order History</h3>";

    //Everthing on the Bottom Panel is the Order History Information.
    $orders = readOrders($db, "%%", "", "", "", "", $user['id'], "", "");
    
    if(count($orders) == 0){
      echo "<p>This user have not placed any orders.</p>";
    } else{
      echo "
<table class='orderhistory'>
  <tr>
    <th style='width:150px;'>Invoice Number</th>
    <th>Order Date</th>
  </tr>
";
      foreach($orders as $order) {
	echo "
<tr>
  <td><a href='order_detail.php?oid={$order['id']}'>{$order['id']}</a></td>
  <td>{$order['order_date']}</td>         
</tr>
";
      }
      echo "</table>";
    }
  
    echo "
  </div>
</div>
";
  }
}

/*
  showOrderTable
  $db: mysqli
 */
function showOrderTable($db, $iid) {
  echo "<table class='order'>\n
          <tr>\n
          <th>Invoice Number</th>\n
          <th>Order Date</th>\n
          <th>Customer Name</th>\n
          <th>Phone Number</th>\n
          <th>Drop Store Location</th>\n
          <th>Status</th>\n
        </tr>\n";
  $orders = readOrders($db, "%%", "", "", "", "", "", "", "");

  //Search Script

  foreach($orders as $order) {
    if (!isset($iid)){
      $customer = readUser($db, $order['customer_id'], false);
    } else {
      $customers = readUsers($db, $order['customer_id'], $iid[4], $iid[0], $iid[1], $iid[5], $iid[2], "", "");
      
      $address = readAddresses($db, $order['addrID'], $iid[3], "", "", "", "");
      
      if(count($customers) == 1 && count($address) == 1){
	$customer = $customers[0];
      } else {
	$customer = NULL;
      }
    }
    if(isset($customer)){
      echo "
  <tr>\n
  <td><a href='order_detail.php?oid={$order['id']}'>{$order['id']}</a></td>
  <td>{$order['order_date']}</td>
  <td>{$customer['first_name']} {$customer['last_name']}</td>
  <td>{$customer['phone_number']}</td>
  <td>{$order['location_id']}</td>
  <td>{$order['order_status']}</td>
  </tr>\n";
    }
  }
  echo "</table>";
}

?>