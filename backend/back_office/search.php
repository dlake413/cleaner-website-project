<!--include all the php search functions and displays.-->
<?php
//DEFINE
/*
  showUserTable: User Search Function
  db: mysqli
  pfn: first name to search
  pln: last name to search
  pem: email to search
  pnn: phonenumber to search
  pug: usergroup search
*/
function showUserTable($db, $pfn, $pln, $pem, $ppn, $pug) {
  
  echo "<table class='user'>\n
          <tr>\n
            <th>First Name</th>\n
            <th>Last Name</th>\n
            <th>Phone Number</th>\n
            <th>E-Mail</th>\n
            <th>User Type</th>\n
            <th>Join Date</th>\n
            <th>Detail</th>\n
          </tr>\n";
        
  //extract data;

  $pfn = "%{$pfn}%";
  $pln = "%{$pln}%";
  $pem = "%{$pem}%";
  $ppn = "%{$ppn}%";
  $pug = "%{$pug}%";
  $result = $db->prepare("SELECT id, first_name, last_name, email, phone_number, user_group, created_on FROM users
                                  WHERE first_name LIKE ?
                                  AND last_name LIKE ?
                                  AND email LIKE ?
                                  AND phone_number LIKE ?
                                  AND user_group LIKE ?");
  $result->bind_param("sssss", $pfn, $pln, $pem, $ppn, $pug);
  $result->execute();
  $result->bind_result($id, $fn, $ln, $em, $pn, $ug, $join);
  
  while($result->fetch()){
    if($ug == 'a'){
      $ug = "Administrator";
    }
    else if($ug == 'w'){
      $ug = "Worker";
    }
    else {
      $ug = "Customer";
    }
    echo "<tr>\n
            <td>{$fn}</td>\n
            <td>{$ln}</td>\n
            <td>{$pn}</td>\n
            <td>{$em}</td>\n
            <td>{$ug}</td>\n
            <td>{$join}</td>\n
            <td><center><a href='user_detail.php?uid={$id}'>View</a></center></td>
          </tr>\n";
  }
  
  echo "</table>";
  $result->close();
}

/*
  
*/

function showOrderDetail($db, $oid){

  $result = $db->prepare("
SELECT orders.*, address.*, invoices.*, services.*
FROM orders 
LEFT JOIN address ON orders.addrID = address.addrID 
LEFT JOIN invoices ON orders.id = invoices.orders_id 
LEFT JOIN services ON services.id = invoices.service_id
WHERE orders.id = ?
");
  $result->bind_param("s", $oid);
  $result->execute();
}

function showUserDetail($db, $uid, $address){

  $result = $db->prepare("
SELECT users.id, users.username, users.first_name, users.last_name, users.email, users.phone_number, users.user_group, users.created_on, users.activeflg, orders.id, orders.order_date
FROM users 
LEFT JOIN orders ON users.id = orders.customer_id
WHERE users.id = ?
");
  $result->bind_param("i", $uid);
  $result->execute();
  $result->bind_result($id, $un, $fn, $ln, $em, $pn, $ug, $join, $active, $oid, $ood);
  $result->fetch();

  if($ug == 'a'){
    $ug = 'Administrator';
  } else if($ug == 'w'){
    $ug = 'Worker';
  } else {
    $ug = 'User';
  }

  if($active){
    $active = "<span style='color:#0F0'><b>Active</b></span>";
  } else {
    $active = "<span style='color:#F00'><b>Inactive</b></span>";
  } 

  //Everything on the Left Panel is the User Information
  echo 
"
  <h3>User Information</h3>
  <div class='user_info'>
  <div class='left'>
    <p><span class='column'><b>Name</b></span> <span class='value'>{$fn} {$ln}</span></p>
    <p><span class='column'><b>Email</b></span> <span class='value'>{$em}</span></p> 
    <p><span class='column'><b>Phone Number</b></span> <span class='value'>{$pn}</span></p>
";
  foreach($address as $ad){
    echo "<div class='addressline'><p><span class='column'><b>Address</b></span>";
    echo "<span class='value'>{$ad['st1']}<br>{$ad['st2']}<br>{$ad['city']}, {$ad['state']}<br>{$ad['zip']}</span>";
    echo "</p></div>";
  }

  //Everything on the Right Panel is the Account Information
  echo "
  </div>
  <div class='right'>
    <p><span class='column'><b>Username</b></span> <span class='value'>{$un}</span></p>
    <p><span class='column'><b>Level</b></span> <span class='value'>{$ug}</span></p>
    <p><span class='column'><b>Sign-up Date</b></span> <span class='value'>{$join}</span></p>
    <p><span class='column'><b>Status</b></span> <span class='value'>{$active}</span></p>
  </div>
  </div>

<div class='orderhistory'>";

  //Everthing on the Bottom Panel is the Order History Information.
  if(is_null($oid)){
    echo "<p>This user have not placed any orders.</p>";
  } else{
    echo "
<table class='orderhistory'>
  <tr>
    <th style='width:150px;'>Invoice Number</th>
    <th>Order Date</th>
  </tr>
";
    do {
      echo "
<tr>
  <td><a href='#'>{$oid}</a></td>
  <td>{$ood}</td>         
</tr>
";
    } while ($result->fetch());
    echo "</table>";
  }

  echo "
  </div>
</div>
";
  $result->close();
}

function showUserAddress($db, $uid) {
  $result = $db->prepare("
SELECT address.street_address, address.street_address2, address.city, address.state, address.zip, userAddr.isPrimary
FROM userAddr
LEFT JOIN address ON userAddr.addrID = address.addrID
WHERE userAddr.userID = ?
");
  $result->bind_param("i", $uid);
  $result->execute();
  $result->bind_result($st1, $st2, $city, $state, $zip, $prim);
  $array = array();
  
  while($result->fetch()){
    $data = array(
		  "st1" => $st1,
		  "st2" => $st2,
		  "city" => $city,
		  "state" => $state,
		  "zip" => $zip,
		  "prim" => $prim);
    $array[] = $data;
  }

  $result->close();
  return $array;
}

/*
  showOrderTable
  $db: mysqli
  $iid: invoid number to search (refer to order.id)
 */
function showOrderTable($db, $iid) {
  echo "<table class='user'>\n
          <tr>\n
          <th>Invoice Number</th>\n
          <th>Order Date</th>\n
          <th>Customer Name</th>\n
          <th>Phone Number</th>\n
          <th>Drop Store Location</th>\n
          <th>Status</th>\n
        </tr>\n";
  
  $iid = "%{$iid}%";
  $result = $db->prepare("SELECT orders.id, orders.order_date, orders.customer_id, orders.location_id, orders.order_status, users.first_name, users.last_name, users.phone_number FROM orders, users WHERE orders.id LIKE ? AND users.id = orders.customer_id");
  $result->bind_param("s", $iid);
  $result->execute();
  $result->bind_result($id, $od, $cus, $loc, $stat, $fn, $ln, $pn);
  
  while($result->fetch()){
    echo "
  <tr>\n
  <td><a href='#'>{$id}</a></td>
  <td>{$od}</td>
  <td>{$fn} {$ln}</td>
  <td>{$pn}</td>
  <td></td>
  <td>{$stat}</td>
  </tr>\n";
  }
  echo "</table>";
  $result->close();
}
?>