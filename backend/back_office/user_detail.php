<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/user_detail.css">
</head>
<body>
<?php

include_once 'db.php';
include_once '../authentication.php';

session_start();

if(logincheck($db)){
  if($_SESSION['ug'] == 'a' || $_SESSION['ug'] == 'w'){
    include("view.php");
    include_once('header.php');

    $url = htmlspecialchars($_SERVER["PHP_SELF"]);

    echo "
<div class='search'>
<form method='get' action='{$url}'>
  USER ID: <input type='number' name='uid'>
</form>
</div>";

    if(isset($_SERVER["REQUEST_METHOD"])){

      if($_SERVER["REQUEST_METHOD"] == "GET"){

	if (!empty($_GET["uid"])){
	  showUserDetail($db, $_GET["uid"]);
	} else {
	  echo "Please enter the user id";
	}
      }
    } else {
      echo "Please enter the user id";
    }
  } else {
    echo "<p>You are not authorized to view this page. Please talk to the administrator.</p>";
  }
} else {

  echo "
<p>This page is protected and login is required. Please log in</p>
<a href='../login.php'>Log In</a>";

}

?>

</body>
</html>
