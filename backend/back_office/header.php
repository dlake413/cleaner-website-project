<?php
include_once '../authentication.php';
include_once 'db.php';

session_start();

if(isset($_SESSION['ug'])){
$ug = $_SESSION['ug'];
}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/header.css">
</head>
<body>
<?php
if(logincheck($db)){
  if($ug == 'a' || $ug == 'w'){
    include_once 'view.php';
    $user_id = $_SESSION['user_id'];
    $session_user = readUser($db, $user_id, true);
    echo "
<div class='header'>
<div class='user'>
  <p>You are logged in as: </p>
  <p>{$session_user['first_name']} {$session_user['last_name']}</p>";

  }
}
?>
  <a href='../../logout.php'>logout</a>
</div>
   <nav class='menu-table'>
   <a class='menu' href='order_data.php'>Orders</a>
   <a class='menu' href='user_data.php'>Users</a>
   <a class='menu' href='#'>Update Orders</a>
   </nav>
</div>
</body>
</html>
