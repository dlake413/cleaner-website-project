<?php

include_once 'db.php';
include_once 'DAL.php';

session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

if(isset($_SERVER['REQUEST_METHOD'])){
  
  if($_POST['action'] == 'addressChange'){
    if($_POST['selectAddress'] == ""){
      
    } else {
      $addrID = $_POST["selectAddress"];
      $order = readOrder($db, $_SESSION['editorderid']);
      updateOrder($db, $_SESSION['editorderid'], $order['order_status'], $order['order_date'], $order['finish_date'], $order['pickup_type'], $order['extra_charge'], $order['discount_amount'], $order['total_price'], $order['pay_method'], $order['order_comment'], $order['activeflg'], $order['customer_id'], $addrID, $order['location_id']);
    }

    header('Location: order_edit.php?oid='.$_SESSION['editorderid']);
  }
  
}

?>