<?php

include('../authentication.php');
include_once 'db.php';

if(isset($_POST['searchby'])){
  $searchstring = array();
  for($i = 0; $i < 9; $i += 1){
    if($i == (int)$_POST['searchby']){
      echo $_POST['searchtexto'];
      $searchstring[$i] = $_POST['searchtext'];
    } else {
      $searchstring[$i] = "";
    }
  }
}

session_start();

?>
<!DOCTYPE html>

<html>
  <head>
    <link rel="stylesheet" type="text/css" href="css/order_data.css">
  </head>
  <body>
<?php
  if(logincheck($db)){
    if($_SESSION['ug'] == 'a' || $_SESSION['ug'] == 'w'){
      include('view.php');
      include_once('header.php');  
      ?>

    <div class='ordersearch'>
      <form method='post'>
        <p style="margin:2.5px 5px 2.5px 2.5px; float:left;">Search </p>
        <select name='searchby'>
          <option value='0'>First Name</option>
          <option value='1'>Last Name</option>
          <option value='2'>Phone Number</option>
          <option value='3'>Street Address</option>
          <option value='4'>Username</option>
          <option value='5'>Email</option>
        </select>
        <input type='text' name='searchtext'/>
        <input type='submit' value='Search'/>
      </form>
    </div>
    <center>

      <?php

      if(isset($searchstring)){
	showOrderTable($db, $searchstring);
      } else {
	showOrderTable($db, NULL);
      }
      
    } else {
      echo "
              <p>You are not authorized to view this page. Please talk to the administrator.</p>";
    }
  } else {
    echo "
            <p>This page is protected and login is required. Please log in</p>
            <a href='../login.php'>Log In</a> ";
  }
?>
    </center>
  </body>
</html>