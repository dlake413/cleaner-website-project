<?php

include_once '../authentication.php';
include_once 'db.php';

session_start();

?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/order_detail.css">
<script>

function showAddressList()
{
  var alist = document.getElementById('useraddress')
  if(alist.style.display == 'block'){
    alist.style.display = 'none';
  } 
  else {
    alist.style.display = 'block';
  }
}

function addNewItem(i)
{
  var addrow = document.createElement('tr');
  addrow.id = 'service'.concat(i.toString()); 
  var indextd = document.createElement('td');
  indextd.appendChild(document.createTextNode(i));
  indextd.name = 'index'+i;
  
  var servicetd = document.createElement('td');
  servicetd.name = 'serviceid' + i;

  var numbertd = document.createElement('td');
  var numberinput = document.createElement('input');
  numberinput.name = 'number' + i;
  numberinput.type = 'number';
  numberinput.step = 1;
  numberinput.min = 0;
  numberinput.className = 'numberInput';
  numbertd.appendChild(numberinput);

  var chargetd = document.createElement('td');
  chargetd.id = 'charge'+i;

  var deletetd = document.createElement('td');
  var deletechk = document.createElement('input');
  deletechk.type = 'checkbox';
  deletechk.name = 'delete' + i;
  deletetd.appendChild(deletechk);

  addrow.appendChild(indextd);
  addrow.appendChild(servicetd);
  addrow.appendChild(numbertd);
  addrow.appendChild(chargetd);
  addrow.appendChild(deletetd);
  var newlinebutton = document.getElementById('newItem');
  newlinebutton.parentNode.insertBefore(addrow, newlinebutton);
  var button = document.getElementById('addbutton');
  button.onclick = function() { addNewItem(i+1) };
}
</script>
</head>
<body>
<?php

if(logincheck($db)){
  if($_SESSION['ug'] == 'a' || $_SESSION['ug'] == 'w'){
    include("view.php");
    include_once('header.php');
    
    $url = htmlspecialchars($_SERVER['PHP_SELF']);
    
    if(isset($_SERVER['REQUEST_METHOD'])){

      if ($_SERVER['REQUEST_METHOD'] == 'GET'){
	$_SESSION['editorderid'] = $_GET["oid"];
	if (!empty($_GET["oid"])) {
	  orderEdit($db, $_GET["oid"]);
	} else {
	  echo "<p>Invalid Request</p>";
	}
      }
    } else {
      echo "Please Enter the invoice number";
    }
  } else {
    echo "<p>You are not authorized to view this page. Please talk to the administrator.</p>";
  }
} else {

  echo "<p>This page is protected and login is required. Please log in</p>
<a href='../login.php'>Log In</a> ";

}
?>
</body>
</html>