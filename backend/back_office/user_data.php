<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="css/user_data.css"> 
    <style>
    </style>
  </head>
  <body>
      
<?php   
include_once '../authentication.php';
include_once 'db.php';

session_start();

if(logincheck($db)){
  if($_SESSION['ug'] == 'a' || $_SESSION['ug'] == 'w'){
    include('view.php');
    include_once('header.php');
    echo "<center>";
    showUserTable($db, "", "", "", "", "", "");
    echo "</center>";
  } else {
    echo "<p>You are not authorized to view this page. Please talk to the administrator.</p>";
  }
} else {

  echo "
<p>This page is protected and login is required. Please log in</p>
<a href='../login.php'>Log In</a>";

}
?>
      
    </center>
  </body>
</html>
