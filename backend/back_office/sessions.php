<?php
include_once 'testsetting.php';

function secured_session_start(){
  $session_name = 'ws-session-id';
  $secure = SECURE;
  
  $httponly = true;

  if(ini_set('session.use_only_cookies', 1) === false){
    header("Location: error.php");
    exit();
  }

  $cookieParams = session_get_cookie_params();
  session_set_cookie_params($cookieParams['path'],
			    $cookieParams['domain'],
			    $secure,
			    $httponly);
  
  session_name($session_name);
  session_start();
  session_regenerate_id();
}

function login($username, $password, $db){
  
  if($stmt = $db->prepare("SELECT id, username, password, salt, user_group FROM users WHERE username = ? LIMIT 1")){
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->store_result();
    
    $stmt->bind_result($user_id, $dbusername, $dbpassword, $salt, $ug);
    $stmt->fetch();

    $password = hash('sha512', $password . $salt);
    if($stmt->num_row == 1){
      
      //if(checkbrute()==true) {
      //return false;
      //} else {
      
      if($dbpassword == $password) {
	$user_browser = $_SERVER['HTTP_USER_AGENT'];
	
	$_SESSION['LePressing_user_id'] = $user_id;
	$_SESSION['LePressing_user_name'] = $dbusername;
	$_SESSION['LePressing_login_string'] = hash('sha512', $password . $user_browser);
	$_SESSION['LePressing_user_group'] = $ug;

	return true;
      } else {

	return false;

      }
      //}
    } else {

    return false;

    }
  }
}

function login_check($db){

  if (isset($_SESSION['LePressing_user_id'], $_SESSION['LePressing_user_name'],
	    $_SESSION['LePressing_login_string'], $_SESSION['LePressing_user_group'])){
    
    $user_id = $_SESSION['LePressing_user_id'];
    $login_string = $_SESSION['LePressing_login_string'];
    $username = $_SESSION['LePressing_user_name'];
    $usergroup = $_SESSION['LePressing_user_group'];
    
    $user_browser = $_SERVER['HTTP_USER_AGENT'];
    
    if($stmt = $db->prepare("SELECT password FROM users WHERE id = ? LIMIT 1")){
      $stmt->bind_param($user_id);
      $stmt->execute();
      $stmt->store_result();

      if($stmt->num_rows == 1){
	$stmt->bind_result($password);
	$stmt->fetch();
	$login_check = hash('sha512', $password . $user_browser);

	if($login_string == $login_check){
	  return true;
	} else {
	  return false;
	}
      } else {
	return false;
      }
    } else {
      return false;
    }
  } else {
    return false;
  }

}



?>