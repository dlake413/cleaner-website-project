<?php

/*
BY DUKE PARK
BASIC CRUD FOR EACH DATA OBJECT.

createUser($db, $username, $password, $first_name, $last_name, $email, $phone_number, $user_group)
: RETURNS inserted user ID

readUser(mysqli, id, isAdmin?)
: RETURNS associative array of user data, given id. $admin is boolean value. If this is false, then it will erase password field. Only Administrator is allowed to view password.
This may return Error string if there are more than 1 user of 1 id.

readUsers(mysqli, id, username, first_name, last_name, email, phone_number, user_group, created_on)
: RETURNS array of associated array of user data, given the condition.

updateUser(mysqli, id, username, password, first_name, last_name, email, phone_number, user_group, created_on, activeflg)
: RETURNS nothing. Modifies corresponding user data with given values.

deleteUser(mysqli, id)
: RETURNS nothing. Sets corresponding user's activeflg to b'0'(false)

*/

//getUUID : RETURN 38 length character of Unique Identifier
//Unique Identifier does not guarantee 100% unique key, but getting a same UUID will not happen in 500 years. (if happens, grab a lottery)

function getUUID($db){
  $id = $db->prepare("SELECT UUID()");
  $id->execute();
  $id->bind_result($uid);
  $id->fetch();
  $id->close();
  return $uid;
}

//This function puts % on both side of string.
//Use to handle query WHERE ~ LIKE ~ syntax to make sure non-input may get ignored.
function putExpr($string) {
  return "%{$string}%";
}

//USER 

function createUser($db, $username, $password, $first_name, $last_name,
		    $email, $phone_number, $user_group) {
  $result = $db->prepare("
INSERT INTO users (username, password, first_name, last_name, email, phone_number, user_group) 
VALUES (? , ? , ? , ? , ? , ? , ? )
");
	$password = hash('sha512',$password);
  $result->bind_param("sssssss", $username, $password, $first_name, $last_name,
		      $email, $phone_number, $user_group);
  $result->execute();
  $result->close();
  return $db->insert_id;
}

function readUser($db, $id, $admin) {
  
  $result = readUsers($db, $id, "", "", "", "", "", "", "");
  
  if(count($result) == 1){
    $data = $result[0];
    if(!$admin){
      $data["password"] = "";
    }
    
    return $data;  
  } elseif(count($result) > 1) {
    return "THERE IS A FATAL ERROR";
  } else {
    return "THERE IS NO DATA";
  }
}

function readUsers($db, $id, $username, $first_name, $last_name,
		   $email, $phone_number, $user_group, $created_on) {
  $username = putExpr($username);
  $first_name = putExpr($first_name);
  $last_name = putExpr($last_name);
  $email = putExpr($email);
  $phone_number = putExpr($phone_number);
  $user_group = putExpr($user_group);
  $created_on = putExpr($created_on);
  $result = $db->prepare("
SELECT id, username, password, first_name, last_name, email, phone_number, user_group, created_on, activeflg
FROM users
WHERE users.id LIKE ?
AND users.username LIKE ?
AND users.first_name LIKE ?
AND users.last_name LIKE ?
AND users.email LIKE ?
AND users.phone_number LIKE ?
AND users.user_group LIKE ?
AND users.created_on LIKE ?
");
  $result->bind_param("ssssssss", $id, $username, $first_name, $last_name,
		      $email, $phone_number, $user_group, $created_on);
  $result->execute();

  $result->bind_result($uid, $uusername, $upassword, $ufirst_name, $ulast_name, 
		       $uemail, $uphone_number, $uuser_group, $ucreated_on, $uactiveflg);

  $array = array();

  $i = 0;
  while($result->fetch()){    
    $data = array(
		  "id" => $uid,
		  "username" => $uusername,
		  "password" => $upassword,
		  "first_name" => $ufirst_name,
		  "last_name" => $ulast_name,
		  "email" => $uemail,
		  "phone_number" => $uphone_number,
		  "user_group" => $uuser_group,
		  "created_on" => $ucreated_on,
		  "activeflg" => $uactiveflg
		  );
    $array[$i] = $data;
    $i = $i + 1;
  }
  $result->close();

  return $array;
}

function updateUser($db, $id, $username, $password, $first_name, $last_name,
		    $email, $phone_number, $user_group, $created_on, $activeflg) {
  $result = $db->prepare("
UPDATE users
SET username = ?, password = ?, first_name = ?, last_name = ?,
email = ?, phone_number = ?, user_group = ?, created_on = ?, activeflg =?
WHERE users.id = ?
");
  $result->bind_param("ssssssssii", $username, $password, $first_name, $last_name,
		      $email, $phone_number, $user_group, $created_on, $activeflg, $id);
  $result->execute();
  $result->close();
}

function deleteUser($db, $id) {
  $result = $db->prepare("
UPDATE users
SET activeflg = b'0'
WHERE users.id = ?
");
  $result->bind_param("i", $id);
  $result->execute();
  $result->close();
}

//ORDERS
function createOrder($db, $finish_date, $pickup_type, $extra_charge, $discount_amount,
		     $total_price, $pay_method, $order_comment,
		     $customer_id, $addrID, $locID) {
  $id = date("Ymd") . "%";
  $getNumber = $db->prepare("
SELECT id FROM orders
WHERE id LIKE ?
ORDER BY id DESC LIMIT 1
");
  $getNumber->bind_param("s", $id);
  $getNumber->execute();
  $getNumber->bind_result($id);

  if($getNumber->fetch()){
    $id = $id + 1;
  } else {
    $id = date("Ymd") . "000000";
  }

  $getNumber->close();
  
  $result = $db->prepare("
INSERT INTO orders (
id, finish_date, pickup_type, extra_charge, discount_amount, total_price, pay_method,
order_comment, customer_id, addrID, location_ID
) 
VALUES ( ?, ? , ? , ? , ? , ? , ? , ? , ? , ? , ? )");
  $result->bind_param("isidddisiss", $id, $finish_date, $pickup_type, $extra_charge, 
		      $discount_amount, $total_price, $pay_method, $order_comment,
		      $customer_id, $addrID, $locID);
  $result->execute();
  $result->close();
  return $id;
}

function readOrder($db, $id) {
  $result = readOrders($db, $id, "%%", "", "", "", "", "", "", "");
  
  if(count($result) == 1){
    return $result[0];
  } elseif(count($result) > 1) {
    return "THERE IS A FATAL ERROR";
  } else {
    return "THERE IS NO DATA";
  }
}

function readOrders($db, $id, $order_status, $order_date, $finish_date, $pickup_type, 
		    $customer_id, $addrID, $locID) {
  if($order_status == ""){
    $order_status = putExpr($order_status);
  }
  $order_date = putExpr($order_date);
  $finish_date = putExpr($finish_date);
  $pickup_type = putExpr($pickup_type);
  if($customer_id == ""){
    $customer_id = putExpr($customer_id);
  }
  $addrID = putExpr($addrID);
  $locID = putExpr($locID);
  
  echo "";

  $result = $db->prepare("
SELECT id, order_status, order_date, finish_date, pickup_type, extra_charge, discount_amount, total_price, pay_method, order_comment, activeflg, customer_id, addrID, location_id 
FROM orders
WHERE orders.id LIKE ?
AND order_status LIKE ?
AND order_date LIKE ?
AND finish_date LIKE ?
AND pickup_type LIKE ?
AND customer_id LIKE ?
AND addrID LIKE ?
AND location_id LIKE ?
");

  $result->bind_param("ssssssss",$id, $order_status, $order_date, $finish_date, 
		      $pickup_type, $customer_id, $addrID, $locID);
  $result->execute();
  $result->bind_result($uid, $uorder_status, $uorder_date, $ufinish_date, $upickup_type, 
		       $extra_charge, $discount_amount, $total_price, $pay_method,
		       $order_comment, $activeflg, $ucustomer_id, $uaddrID, $ulocID);
  
  $array = array();
  $i = 0;
  while($result->fetch()){
    $data = array(
		  "id" => $uid,
		  "order_status" => $uorder_status, 
		  "order_date" => $uorder_date, 
		  "finish_date" => $ufinish_date,
		  "pickup_type" => $upickup_type, 
		  "extra_charge" => $extra_charge,
		  "discount_amount" => $discount_amount,
		  "total_price" => $total_price,
		  "pay_method" => $pay_method,
		  "order_comment" => $order_comment,
		  "activeflg" => $activeflg,
		  "customer_id" => $ucustomer_id,
		  "addrID" => $uaddrID, 
		  "location_id" => $ulocID		  
		  );
    $array[$i] = $data;
    $i = $i + 1;
  }
  $result->close();

  return $array;
}

function updateOrder($db, $id, $order_status, $order_date, $finish_date, $pickup_type, 
		     $extra_charge, $discount_amount, $total_price, $pay_method,
		     $order_comment, $activeflg, $customer_id, $addrID, $locID) {
  $result = $db->prepare("
UPDATE orders
SET order_status = ?,
order_date = ?,
finish_date = ?,
pickup_type = ?,
extra_charge = ?,
discount_amount = ?,
total_price = ?,
pay_method = ?,
order_comment = ?,
activeflg = ?,
customer_id = ?,
location_id = ?,
addrID = ?
WHERE orders.id = ?
");
  $result->bind_param("issidddisiisss", $order_status, $order_date, $finish_date,
		      $pickup_type, $extra_charge, $discount_amount, $total_price, 
		      $pay_method, $order_comment, $activeflg, $customer_id, $locID, 
		      $addrID, $id);
  $result->execute();
  $result->close();
}

function deleteOrder($db, $id) {
  $result = $db->prepare("
UPDATE orders
SET activeflg = b'0'
WHERE orders.id = ?
");
  $result->bind_param("s", $id);
  $result->execute();
  $result->close();
}

//ADDRESS
function createAddress($db, $st1, $st2, $city, $state, $zip) {
  $id = getUUID($db);
  $result = $db->prepare("
INSERT INTO address (addrID, street_address, street_address2, city, state, zip)
VALUES(?, ?, ?, ?, ?, ?)
");
  $result->bind_param("sssssi", $id, $st1, $st2, $city, $state, $zip);
  $result->execute();
  $result->close();
  return $id;
}

function readAddress($db, $id) {
  $result = readAddresses($db, $id, "", "", "", "", "");
  
  if(count($result) == 1){
    return $result[0];
  } elseif(count($result) > 1) {
    return "THERE IS A FATAL ERROR";
  } else {
    return "THERE IS NO DATA";
  }
}

function readAddresses($db, $id, $st1, $st2, $city, $state, $zip) {
  $st1 = putExpr($st1);
  $st2 = putExpr($st2);
  $city = putExpr($city);
  $state = putExpr($state);
  $zip = putExpr($zip);
  
  $result = $db->prepare("
SELECT addrID, street_address, street_address2, city, state, zip, activeflg FROM address
WHERE addrID LIKE ?
AND street_address LIKE ?
AND street_address2 LIKE ?
AND city LIKE ?
AND state LIKE ?
AND zip LIKE ?
");
  $result->bind_param("ssssss", $id, $st1, $st2, $city, $state, $zip);
  $result->execute();
  $result->bind_result($uid, $ust1, $ust2, $ucity, $ustate, $uzip, $activeflg);
  
  $array = array();
  $i = 0;
  while($result->fetch()){
    $data = array(
		  "addrID" => $uid,
		  "street_address" => $ust1,
		  "street_address2" => $ust2,
		  "city" => $ucity,
		  "state" => $ustate,
		  "zip" => $uzip,
		  "activeflg" => $activeflg
		  );
    $array[$i] = $data;
    $i = $i + 1;
  }
  $result->close();

  return $array;
}

function updateAddress($db, $id, $st1, $st2, $city, $state, $zip, $activeflg) {
  $result = $db->prepare("
UPDATE address
SET street_address = ?,
street_address2 = ?,
city = ?,
state = ?,
zip = ?,
activeflg = ?
WHERE addrID = ?
");
  $result->bind_param("ssssiis", $st1, $st2, $city, $state, $zip, $activeflg, $id);
  $result->execute();
  $result->close();
}

function deleteAddress($db, $id) {
  $result = $db->prepare("
UPDATE address
SET activeflg = b'0'
WHERE addrID = ?
");
  $result->bind_param("s", $id);
  $result->execute();
  $result->close();

}

//USERADDR
function createUserAddr($db, $uid, $aid) {
  $result = $db->prepare("
INSERT INTO userAddr (userID, addrID)
VALUES (?, ?)
");
  $result->bind_param("ss", $uid, $aid);
  $result->execute();
  $result->close();
}

function readUserAddr($db, $uid, $aid) {
  $result = readUserAddrs($db, $uid, $aid);
  
  if(count($result) == 1){
    return $result[0];
  } elseif(count($result) > 1){
    return "FATAL ERROR";
  } else {
    return "THERE IS NO DATA";
  }
}

function readUserAddrs($db, $uid, $aid) {
  if($uid == ""){
    $uid = putExpr($uid);
  }
  if($aid == ""){
    $aid = putExpr($aid);
  }
  
  $result = $db->prepare("
SELECT userID, addrID, isPrimary FROM userAddr
WHERE userID LIKE ?
AND addrID LIKE ?
");
  $result->bind_param("ss", $uid, $aid);
  $result->execute();
  $result->bind_result($userID, $addrID, $isPrimary);
  
  $array = array();
  $i = 0;
  while($result->fetch()){
    $data = array(
		  "userID" => $userID,
		  "addrID" => $addrID,
		  "isPrimary" => $isPrimary
		  );
    $array[$i] = $data;
    $i += 1;
  }
  $result->close();
  
  return $array;
}

function updateUserAddr($db, $uid, $aid, $isP) {
  $result = $db->prepare("
UPDATE userAddr
SET isPrimary = ?
WHERE userID = ?
AND addrID = ?
");
  $result->bind_param("iss", $isP, $uid, $aid);
  $result->execute();
  $result->close();
}

//THIS WONT WORK IF DB USER HAS NO GRANT OPTION DELETE.
function deleteUserAddr($db, $uid, $aid) {
  $result = $db->prepare("
DELETE FROM userAddr
WHERE userID = ?
AND addrID = ?
");

  $result->bind_param("ss", $uid, $aid);
  $result->execute();
  $result->close();
}

//INVOICES
function createInvoice($db, $total_price, $orderID, $number_items, $service_id) {
  $id = getUUID();
  $result = $db->prepare("
INSERT INTO invoices (id, total_price, number_items, service_id, orderID)
VALUES (?, ?, ?, ?, ?)
");
  $result->bind_param("sdiii", $id, $total_price, $number_items, $service_id, $orderID);
  $result->execute();
  $result->close();

  return $id;
}

function readInvoice($db, $id) {
  $result = readInvoices($db, $id, "", "", "");
  
  if(count($result) == 1) {
    return $result[0];
  } elseif(count($result) > 1) {
    echo "THERE IS FATAL ERROR";
  } else {
    echo "THERE IS NO DATA";
  }
}

function readInvoices($db, $id, $service_id, $activeflg, $orderID) {
  $service_id = putExpr($service_id);
  if($activeflg = ""){
    $activeflg = putExpr($activeflg);
  }
  $orderID = putExpr($orderID);
  
  $result = $db->prepare("
SELECT id, total_price, number_items, service_id, activeflg, orders_id FROM invoices
WHERE id LIKE ?
AND service_id LIKE ?
AND activeflg LIKE ?
AND orders_id LIKE ?
");
  $result->bind_param("ssss", $id, $service_id, $activeflg, $orderID);
  $result->execute();
  $result->bind_result($iid, $total_price, $number_items, $iservice_id, $iactiveflg,
		       $iorderID);
  
  $array = array();
  $i = 0;
  while($result->fetch()) {
    $data = array(
		  "id" => $iid,
		  "total_price" => $total_price,
		  "number_items" => $number_items,
		  "service_id" => $iservice_id,
		  "activeflg" => $iactiveflg,
		  "orderID" => $iorderID
		  );
    $array[$i] = $data;
  };
  $result->close();
  
  return $array;
}

function updateInvoice($db, $id, $total_price, $number_items, $service_id, $activeflg, 
		       $orderID) {
  $result = $db->prepare("
UPDATE invoices
SET total_price = ?,
number_items = ?,
service_id = ?,
activeflg = ?,
orderID = ?
WHERE id = ?
");
  $result->bind_param("diiiis", $total_price, $number_items, $service_id, $activeflg,
		      $orderID, $id);
  $result->execute();
  $result->close();
}

function deleteInvoice($db, $id) {
  $result = $db->prepare("UPDATE invoices SET activeflg = b'0' WHERE id = ?");
  $result->bind_param("s", $id);
  $result->execute();
  $result->close();
}

//SERVICES
function createService($db, $type_service, $price) {
  $result = $db->prepare("
INSERT INTO services (type_service, price)
VALUES (?, ?)
");
  $result->bind_param("sd", $type_service, $price);
  $result->execute();
  return $db->insert_id;
}

function readService($db, $id) {
  
  $services = readServices($db, $id, "", "");
  
  if(count($services) == 1){
    return $services[0];
  } else {
    return "Either requested ID {$id} does not exist or there is a problem in databases";
  }
  
}

function readServices($db, $id, $type_service, $price) {
  if($id == ""){
    $id = putExpr($id);
  }
  $type_service = putExpr($type_service);
  $price = putExpr($price);

  $result = $db->prepare("
SELECT * FROM services WHERE id LIKE ? AND type_service LIKE ? AND price LIKE ?
");
  $result->bind_param("sss", $id, $type_service, $price);
  $result->execute();
  
  $resultset = $result->get_result();
  $i = 0;
  $services = array();

  while($service = $resultset->fetch_array(MYSQLI_ASSOC)){
    $services[$i] = $service;
    $i += 1;
  }
  
  return $services;
}

function updateService($db, $id, $type_service, $price) {
  $result = $db->prepare("
UPDATE services
SET type_services = ?, price = ?
WHERE id = ?
");
  $result->bind_param("sdi",$type_service,$price,$id);
  $result->execute();
}
function deleteService() {
}

?>
