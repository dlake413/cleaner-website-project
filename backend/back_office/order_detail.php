<?php

include_once '../authentication.php';
include_once 'db.php';

session_start();
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/order_detail.css">
</head>
<body>
<?php
if(logincheck($db)){
  if($_SESSION['ug'] == 'a' || $_SESSION['ug'] == 'w'){
    include("view.php");
    include_once('header.php');
    
    $url = htmlspecialchars($_SERVER['PHP_SELF']);
    
    echo "
<div class='search'>
<form method='get' action='{$url}'>
  INVOICE NUMBER: <input type='number' name='oid'>
</form>
</div>";

    if(isset($_SERVER['REQUEST_METHOD'])){

      if ($_SERVER['REQUEST_METHOD'] == 'GET'){

	if (!empty($_GET["oid"])) {
	  showOrderDetail($db, $_GET["oid"]);
	  echo "
<div style='margin-left:100px; width:900px'>
<hr>
<a href='order_edit.php?oid={$_GET['oid']}'>Edit Order</a>
</div>
";
	} else {
	  echo "<p>Please Enter the invoice number</p>";
	}
      }
    } else {
      echo "Please Enter the invoice number";
    }
  } else {
    echo "<p>You are not authorized to view this page. Please talk to the administrator.</p>";
  }
} else {

  echo "<p>This page is protected and login is required. Please log in</p>
<a href='../login.php'>Log In</a> ";

}
?>
</body>
</html>