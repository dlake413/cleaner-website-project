<?php

include_once 'back_office/settings.php';

function login($db, $username, $password) {
  if($stmt = $db->prepare("SELECT id, username, password, user_group FROM users WHERE username = ? LIMIT 1")){
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($id, $dbusername, $dbpassword, $ug);
    $stmt->fetch();

    if($stmt->num_rows == 1){

      if($password == $dbpassword){
	$user_browser = $_SERVER['HTTP_USER_AGENT'];
	$_SESSION['user_id'] = $id;
	$_SESSION['ug'] = $ug;
	$_SESSION['login_string'] = hash('sha512', $password . $user_browser);
	$stmt->close();
	return true;

      } else {

	return false;

      }

    } else {
      return false;
    }
      
  }
}

function logincheck($db)
{
  if(isset($_SESSION['user_id'])) {
    $userid = $_SESSION['user_id'];
    $login_string = $_SESSION['login_string'];

    if($stmt = $db->prepare("SELECT password FROM users WHERE id = ? LIMIT 1")){
      $stmt->bind_param("s", $userid);
      $stmt->execute();
      $stmt->store_result();

      $stmt->bind_result($password);
      $stmt->fetch();
      
      $user_browser = $_SERVER['HTTP_USER_AGENT'];
      $login_check = hash('sha512', $password . $user_browser);
      $stmt->close();

      if ($login_check == $login_string){
	return true;
      }
    } else {

      return false;

    }

  } else {

    return false;

  }
}

?>