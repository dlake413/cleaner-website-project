<?php

include_once 'authentication.php';
include_once 'back_office/db.php';

session_start();

if(isset($_POST['username'], $_POST['p'])){
  $username = $_POST['username'];
  $password = $_POST['p'];
  
  if(login($db, $username, $password)){
    if($_SESSION['FROM'] == 'workers'){
      unset($_SESSION['FROM']);
      header("Location: back_office/order_data.php");
    } else {
      header("Location: ../");
    }
  } else {
    $_SESSION['ERROR'] = "login failed";
    header("Location: ../login.php");
  }
} else {
  $_SESSION['ERROR'] = "Invalid Request";
  header("Location: backoffice/order_data.php");
}

?>