<?php

include_once "back_office/DAL.php";
include_once "back_office/db.php";

session_start();
if(isset($_SESSION['userinfodata'], $_SESSION['addressinfodata'])){
  $requiredfilledup = true;
  $uinfo = $_SESSION['userinfodata'];
  $ainfo = $_SESSION['addressinfodata'];
  unset($_SESSION['userinfodata']);
  unset($_SESSION['addressinfodata']);
  $previous_post = array();
  $error_post = array();
  for($i = 0; $i < count($uinfo); $i += 1){
    if($uinfo[$i] == ""){
      $error_post[$i] = "Required";
      $requiredfilledup = false;
    }
    //PASSWORD MATCHING
    elseif($i == 1){
      if($uinfo[$i] != $_SESSION['repass']){
	$error_post[11] = "Password Mismatch";
	$requiredfilledup = false;
	unset($_SESSION['repass']);
      } else {
	$error_post[11] = "";
      }
    }
    //EMAIL VALIDATION
    elseif($i == 4){
      if(!filter_var($uinfo[$i], FILTER_VALIDATE_EMAIL)){
	$error_post[$i] = "Invalid Email";
	$requiredfilledup = false;
      } else {
	$error_post[$i] = "";
      }
    } else {
      $error_post[$i] = "";
    }
    
    if($i != 1){
      $previous_post[$i] = $uinfo[$i];
    }

  }
  for($i = 0; $i < count($ainfo); $i += 1){
    if($ainfo[$i] == "" && $i != 1){
      $error_post[$i + 6] = "Required";
      $requiredfilledup = false;
    } else {
      $error_post[$i + 6] = "";
    }
    $previous_post[$i + 6] = $ainfo[$i];
  }

  if ($requiredfilledup){
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
    $uid = createUser($db, $uinfo[0], $uinfo[1], $uinfo[2], $uinfo[3], $uinfo[4], $uinfo[5], 'u');
//    echo "<p>Create Success!</p>";
    $aid = createAddress($db, $ainfo[0], $ainfo[1], $ainfo[2], $ainfo[3], $ainfo[4]);
//    echo "<p>Create Success!</p>";
    createUserAddr($db, $uid, $aid);
//    echo "<p>Create Success!</p>";
    header("Location: ../");
  } else {
    $_SESSION['errorlist'] = $error_post;
    $_SESSION['previouspost'] = $previous_post;
    header("Location: ../signup.php");
  }
} else {
  echo "<p>Invalid Request...</p>";
  echo "<a href='../signup.php'>go back</p>";
}
?>

<?php
header('Refresh: 3;url=../');
?>