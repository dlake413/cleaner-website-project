<!DOCTYPE html>
<?php
include_once 'backend/authentication.php';
include_once 'backend/back_office/db.php';
include_once 'backend/back_office/DAL.php';
?>

<html>
<head>
    <title>Le Pressing</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="slideshow.css" rel="stylesheet" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="mapset.js"></script>
      
    <script src="jssor/jssor.core.js"></script>
    <script src="jssor/jssor.slider.js"></script>
    <script src="jssor/jssor.utils.js"></script>
    <script src="slide.js"></script>
    
    <script>
    function resizeSlider() {
//        var w = $(window).width();
        document.getElementById('asdf').style.width = '1440px';
        alert("i got here");
    }
    </script>

  </head>
<body onload="resizeSlider()">
<div id="container">
    
   <?php include "navigation.php"; ?>
    <div style="width:100%;height:30px;background-color:#000;"></div>
    
    <div id="main1">
    <div id="main1-cont">
        <p style="font-size:1.5em;line-height:1.5em;">Since 1986 <b>Le Pressing</b> (formerly Symphony Cleaners) has been a family owned and operated, full service 
        dry cleaning establishment. With many locations in Midtown and the Upper East Side, 
        our services are recognized for delivering the <b>highest quality</b>, care and courtesy.</p>
    </div>
    </div> 
        
    
    <div id="main2">
    <div id="main2-cont">
        <center>
        <table id="newstable">
            <tr>
                <td style="width:300px;overflow:hidden:">
                    <p><b>Professional Dry Cleaning</b></p>
                    <p>The professional dry cleaning service at Le Pressing keeps your clothes beautiful.</p>
                </td>
                <td>
                    <p><b>Free Pick-up &amp; Delivery</b></p>
                    <p>Complimentary pick-up and delivery in Manhattan. Same day processing available.</p>
                </td>
                <td>
                    <p><b>Announcements</b></p>
                        <ul>Grand Re-openings
                        <li>554 Third Avenue<br />(between 36th and 37th Street)</li>
                        <li>843 Second Avenue<br />(corner of 45th Street)</li>
                        <li>Click <a href="locations.html">here</a> for more locations.</li>
                        </ul>
                </td>            
            </tr>
        </table>
        </center>
    </div>
        
    <div id="main2-cont2"><center>
        <a href="services.php">
        <div id="index-button-container" onclick="">
            <div id="index-button-image" style="background-image:url(images/service-bkgd.jpg);"></div>
            <div id="index-button-hover"></div> 
            <div id="index-button-title"><p>Services</p></div>  
        </div></a>
        <a href="schedule.php">
        <div id="index-button-container">
            <div id="index-button-image" style="background-image:url(images/schedule-bkgd.jpg);"></div>
            <div id="index-button-hover"></div> 
            <div id="index-button-title"><p>Schedule</p></div>  
        </div></a>
        <a href="contact.php">
        <div id="index-button-container">
            <div id="index-button-image" style="background-image:url(images/contact-bkgd.jpg);"></div>
            <div id="index-button-hover"></div> 
            <div id="index-button-title"><p>Contact</p></div>  
        </div></a>
        </center>
        
    </div>
    </div>
    
    <div id="map"></div>
   
	<?php include('footer.php'); ?>     
    
</div>
    

    
</body>
</html>
